####软件架构
01.   SpringBoot最新版
02.   MyBatis
03.   LayUi-MiNi
04.   Kaptcha
05.   MySQL8.0
06.   Maven3.6
07.   Lombok
08.   Shiro
09.   Thymeleaf
10.   EChar
11.   Quartz


####安装教程
01.   安装 JDK，1.8以上
02.   安装 Maven，3.6.0 以上
03.   安装 MySQL，8.0 版本
04.   导入数据库文件到 MySQL，数据库文件放置：db 文件夹中
05.   修改配置文件中数据库的连接信息，链接地址，用户名，密码等信息
06.   首次拉取导入maven依赖
07.   安装IDEA


####项目介绍
01.   协同过滤算法推荐图书
02.   系统模块
03.   用户模块


####显示模块划分
一、用户
   1. 个人中心
   
      01.记录查询
      
   2.  我要分享
   
       01. 上传图书
        
   3.  邮箱
       
       01. 推荐图书
       
   4.  分析(EChar图表分析)
      
       01. 收藏夹分析
       
       20. 各种数据统计
       
   5.  用户设置
             
       01. 权限开启及关闭
       
           a. 评论显示的开启与隐藏
           
           b. 评分的显示与隐藏
           
           c. 是否开启无记录浏览
    
二、书库(核心模块)


三、其他管理
   
   