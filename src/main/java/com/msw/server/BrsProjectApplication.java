package com.msw.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.msw.server.system.mapper","com.msw.server.business.mapper"})
public class BrsProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrsProjectApplication.class, args);
    }

}
