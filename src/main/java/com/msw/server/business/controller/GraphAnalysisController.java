package com.msw.server.business.controller;

import com.msw.server.business.service.IGraphAnalysisService;
import com.msw.server.common.param.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Api(value = "用户数据分析模块接口")
@Slf4j
@Controller
@RequestMapping("/bus")
public class GraphAnalysisController {

    @Autowired
    private IGraphAnalysisService iGraphAnalysisService;

    @ApiOperation(value = "获取一周的浏览记录分析")
    @GetMapping("/getWeekGraphLine")
    @ResponseBody
    public ResponseResult getWeekGraphLine(HttpServletRequest request) throws Exception {
        log.info("正在获取您的一周的浏览记录折线图");
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iGraphAnalysisService.getBrowseWeekGraph(user_id);
        return responseResult;
    }

    @ApiOperation(value = "获得收藏记录玫瑰图")
    @GetMapping("/getCollectRoseGraph")
    @ResponseBody
    public ResponseResult getCollectRoseGraph(HttpServletRequest request) {
        log.info("正在获取您的收藏记录玫瑰图");
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iGraphAnalysisService.getCollectLikeGraph(user_id);
        return responseResult;
    }

    @ApiOperation(value = "指标排序")
    @GetMapping("/sortCollectType")
    @ResponseBody
    public ResponseResult sortCollectTypeRoseGraph(HttpServletRequest request) throws IOException {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iGraphAnalysisService.sortCollectType(user_id);
        return responseResult;
    }

    @ApiOperation(value = "得到类型推荐")
    @GetMapping("/getTypeBookRecommend")
    @ResponseBody
    public ResponseResult getTypeBookRecommend(HttpServletRequest request) throws IOException {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iGraphAnalysisService.getTypeBookRecommend(user_id);
        return responseResult;
    }

}
