package com.msw.server.business.controller;

import com.msw.server.business.entity.*;
import com.msw.server.business.service.IManageRecordService;
import com.msw.server.common.param.DelIdParam;
import com.msw.server.common.param.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Api(value = "个人中心模块接口")
@Controller
@RequestMapping("/bus")
public class ManageRecordController {

    @Autowired
    private IManageRecordService iManageRecordService;


    @ApiOperation(value = "查询用户浏览记录信息")
    @GetMapping("/getBrowseRecords")
    @ResponseBody
    public ResponseResult queryBrowseRecords(int page, int limit, BrowseRecord browseRecord, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        browseRecord.setUser_id(user_id);
        ResponseResult responseResult = iManageRecordService.getBrowseRecords(page, limit, browseRecord);
        return responseResult;
    }

    @ApiOperation(value = "删除用户浏览记录信息")
    @DeleteMapping("/deleteBrowseRecords")
    @ResponseBody
    public ResponseResult deleteBrowseRecords(DelIdParam delIdParam) {
        ResponseResult responseResult = iManageRecordService.deleteBrowseRecords(delIdParam.getIds());
        return responseResult;
    }

    @ApiOperation(value = "创建浏览记录")
    @PostMapping("/createBrowseRecord")
    @ResponseBody
    public ResponseResult createBrowseRecord(String book_id, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageRecordService.createBrowseRecord(book_id, user_id);
        return responseResult;

    }

    @ApiOperation(value = "得到昨日浏览记录")
    @GetMapping("/getYesBrowseRecord")
    @ResponseBody
    public ResponseResult getYesBrowseRecord(HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageRecordService.getYesBrowseRecords(user_id);
        return responseResult;
    }

    @ApiOperation(value = "创建收藏记录")
    @PostMapping("/createCollectRecord")
    @ResponseBody
    public ResponseResult createCollectRecord(String book_id, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageRecordService.createCollectRecord(book_id, user_id);
        return responseResult;
    }

    @ApiOperation(value = "查询用户收藏记录信息")
    @GetMapping("/queryCollectRecords")
    @ResponseBody
    public ResponseResult queryCollectRecords(int page, int limit, CollectRecord collectRecord, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        collectRecord.setUser_id(user_id);
        ResponseResult responseResult = iManageRecordService.getCollectRecords(page, limit, collectRecord);
        return responseResult;
    }

    @ApiOperation(value = "删除用户收藏记录信息/或取消收藏")
    @DeleteMapping("/deleteCollectRecords")
    @ResponseBody
    public ResponseResult deleteCollectRecords(DelIdParam delIdParam) {
        ResponseResult responseResult = iManageRecordService.deleteCollectRecords(delIdParam.getIds(), delIdParam.getOpt());
        return responseResult;
    }

    @ApiOperation(value = "查询用户回收站记录信息")
    @GetMapping("/queryRecycleBins")
    @ResponseBody
    public ResponseResult queryRecycleBins(int page, int limit, RecycleBin recycleBin, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        recycleBin.setUser_id(user_id);
        ResponseResult responseResult = iManageRecordService.getRecycleBins(page, limit, recycleBin);
        return responseResult;
    }

    @ApiOperation(value = "删除用户回收站记录信息/或恢复记录")
    @DeleteMapping("/deleteRecycleBins")
    @ResponseBody
    public ResponseResult deleteRecycleBins(DelIdParam delIdParam, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageRecordService.deleteRecycleBins(delIdParam.getIds(), delIdParam.getOpt(), user_id);
        return responseResult;
    }

    @ApiOperation(value = "创建评论记录")
    @PostMapping("/createCommentRecord")
    @ResponseBody
    public ResponseResult createCommentRecord(String book_id, String content, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageRecordService.createCommentRecord(book_id, user_id, content);
        return responseResult;
    }

    @ApiOperation(value = "查询用户评论记录信息")
    @GetMapping("/queryCommentRecords")
    @ResponseBody
    public ResponseResult queryCommentRecords(int page, int limit, CommentRecord commentRecord, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        commentRecord.setUser_id(user_id);
        ResponseResult responseResult = iManageRecordService.getCommentRecords(page, limit, commentRecord);
        return responseResult;
    }

    @ApiOperation(value = "删除用户评论记录信息")
    @DeleteMapping("/deleteCommentRecords")
    @ResponseBody
    public ResponseResult deleteCommentRecords(DelIdParam delIdParam) {
        ResponseResult responseResult = iManageRecordService.deleteCommentRecords(delIdParam.getIds());
        return responseResult;
    }

    @ApiOperation(value = "更新评论")
    @PutMapping("/updateContent")
    @ResponseBody
    public ResponseResult modifyContent(long id, String content) {
        ResponseResult responseResult = iManageRecordService.modifyContent(id, content);
        return responseResult;
    }


    @ApiOperation(value = "创建评分记录")
    @PostMapping("/createScoreRecord")
    @ResponseBody
    public ResponseResult createScoreRecord(String book_id, int score, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageRecordService.createScoreRecord(book_id, user_id, score);
        return responseResult;
    }

    @ApiOperation(value = "查询用户评分信息")
    @GetMapping("/queryScoreRecords")
    @ResponseBody
    public ResponseResult queryScoreRecords(int page, int limit, ScoreRecord scoreRecord, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        scoreRecord.setUser_id(user_id);
        ResponseResult responseResult = iManageRecordService.getScoreRecords(page, limit, scoreRecord);
        return responseResult;
    }

    @ApiOperation(value = "删除用户评分信息")
    @DeleteMapping("/deleteScoreRecords")
    @ResponseBody
    public ResponseResult deleteScoreRecords(DelIdParam delIdParam) {
        ResponseResult responseResult = iManageRecordService.deleteScoreRecords(delIdParam.getIds());
        return responseResult;
    }

    @ApiOperation(value = "更新评分")
    @PutMapping("/updateScore")
    @ResponseBody
    public ResponseResult modifyScore(long id, int score) {
        ResponseResult responseResult = iManageRecordService.modifyScore(id, score);
        return responseResult;
    }

    @ApiOperation(value = "创建用户上传记录信息")
    @PostMapping("/createUploadRecords")
    @ResponseBody
    public ResponseResult createUploadRecords(UploadRecord uploadRecord, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        uploadRecord.setUser_id(user_id);
        ResponseResult responseResult = iManageRecordService.createUploadRecord(uploadRecord);
        return responseResult;
    }


    @ApiOperation(value = "审核用户上传记录信息")
    @PutMapping("/checkUploadRecord")
    @ResponseBody
    public ResponseResult checkUploadRecord(long id, int check, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageRecordService.passOrNoPass(id, check, user_id);
        return responseResult;
    }

    @ApiOperation(value = "查询用户上传记录信息")
    @GetMapping("/queryUploadRecords")
    @ResponseBody
    public ResponseResult queryUploadRecords(int page, int limit, UploadRecord uploadRecord) {
        ResponseResult responseResult = iManageRecordService.getUploadRecords(page, limit, uploadRecord);
        return responseResult;
    }

    @ApiOperation(value = "删除用户上传记录信息")
    @DeleteMapping("/deleteUploadRecords")
    @ResponseBody
    public ResponseResult deleteUploadRecords(DelIdParam delIdParam) {
        ResponseResult responseResult = iManageRecordService.deleteUploadRecords(delIdParam.getIds());
        return responseResult;
    }

    @ApiOperation(value = "查询用户推荐记录信息")
    @GetMapping("/queryRecommendRecords")
    @ResponseBody
    public ResponseResult queryRecommendRecords(int page, int limit, RecommendRecord recommendRecord, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        recommendRecord.setUser_id(user_id);
        ResponseResult responseResult = iManageRecordService.getRecommendRecords(page, limit, recommendRecord);
        return responseResult;
    }

    @ApiOperation(value = "删除用户推荐记录信息")
    @DeleteMapping("/deleteRecommendRecords")
    @ResponseBody
    public ResponseResult deleteRecommendRecords(DelIdParam delIdParam) {
        ResponseResult responseResult = iManageRecordService.deleteRecommendRecords(delIdParam.getIds());
        return responseResult;
    }

    @ApiOperation(value = "撤销用户推荐记录信息")
    @DeleteMapping("/revokeRecommend")
    @ResponseBody
    public ResponseResult revokeRecommend(long id, String book_id, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageRecordService.revokeRecommend(id, book_id, user_id);
        return responseResult;
    }

}
