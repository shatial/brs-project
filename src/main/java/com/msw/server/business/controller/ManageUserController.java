package com.msw.server.business.controller;

import com.msw.server.business.entity.UserInfo;
import com.msw.server.business.service.IManageUserService;
import com.msw.server.common.param.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Api(value = "用户信息模块接口")
@Controller
@RequestMapping("/bus")
public class ManageUserController {

    @Autowired
    private IManageUserService iManageUserService;

    @ApiOperation(value = "新建/更新用户信息")
    @ResponseBody
    @PostMapping("/perfectUserInfo")
    private ResponseResult perfectUserInfo(UserInfo userInfo, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        userInfo.setUser_id(user_id);
        ResponseResult responseResult = iManageUserService.perfectUserInfo(userInfo);
        return responseResult;
    }

    @ApiOperation(value = "查询用户信息")
    @ResponseBody
    @GetMapping("/getUserInfo")
    public ResponseResult queryUserInfo(HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManageUserService.getUserInfo(user_id);
        return responseResult;
    }

    @ApiOperation(value = "查询父级职业类型")
    @ResponseBody
    @GetMapping("/queryParentWorkType")
    public ResponseResult queryParentWorkType() {
        ResponseResult responseResult = iManageUserService.getParentWorkType();
        return responseResult;
    }

    @ApiOperation(value = "查询子级职业类型")
    @ResponseBody
    @GetMapping("/queryChildWorkType")
    public ResponseResult queryChildWorkType(String parent_code) {
        ResponseResult responseResult = iManageUserService.getChildWorkType(parent_code);
        return responseResult;
    }
}
