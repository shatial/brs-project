package com.msw.server.business.controller;

import com.msw.server.business.entity.BookHouse;
import com.msw.server.business.service.IManagerBookService;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.common.param.SearchParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Api(value = "书库模块接口")
@Slf4j
@Controller
@RequestMapping("/bus")
public class ManagerBookController {
    @Autowired
    private IManagerBookService iManagerBookService;
    @Autowired
    private ManageRecordController manageRecordController;


    @ApiOperation(value = "获得某种类型的图书")
    @GetMapping("/queryBooks")
    @ResponseBody
    public ResponseResult getBooksList(int page, int limit, BookHouse bookHouse, HttpServletRequest request) {
        log.info("正在获取书籍列表");
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManagerBookService.getTypeBooks(page, limit, bookHouse, user_id);
        return responseResult;
    }

    @ApiOperation(value = "进入详情展示页面")
    @RequestMapping("/toBookDetails")
    public String toBookDetails(String book_id, Model model, HttpServletRequest request) {
        log.info("正在查询书号为：" + book_id + "图书的详情");
        String user_id = (String) request.getSession().getAttribute("user_id");
        manageRecordController.createBrowseRecord(book_id, request);
        model.addAttribute("bookId", book_id);
        model.addAttribute("userId", user_id);//为了评论
        return "book/bookDetails";

    }

    @ApiOperation(value = "获得书的详情信息")
    @GetMapping("/getBookDetails")
    @ResponseBody
    public ResponseResult getBookDetails(String book_id, HttpServletRequest request) {
        log.info("书号为：" + book_id + "图书的详情已查出");
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iManagerBookService.getBookDetails(book_id, user_id);
        return responseResult;

    }

    @ApiOperation(value = "进入搜索界面并且携带结果")
    @RequestMapping("/search")
    public String searchBooks(SearchParam searchParam, Model model) {
        List<BookHouse> books = (List<BookHouse>) iManagerBookService.searchBooks(searchParam).getData();
        model.addAttribute("books", books);
        model.addAttribute("book_a_type", searchParam.getBook_a_type());
        if (searchParam.getDesc() != null) {
            log.info("正在根据关键字：" + searchParam.getDesc() + "进行查询");
            model.addAttribute("desc", "关键字" + "'" + searchParam.getDesc() + "'" + "检索到" + books.size() + "个结果");
        } else {
            log.info("正在根据书名/作者名进行查询");
            model.addAttribute("desc", "检索到" + books.size() + "个结果");
        }
        return "book/searchBooks";
    }

    @ApiOperation(value = "获得父级图书类型")
    @GetMapping("/queryParentBookType")
    @ResponseBody
    public ResponseResult queryParentBookType() {
        ResponseResult responseResult = iManagerBookService.getParentBookType();
        return responseResult;
    }

    @ApiOperation(value = "获得子级图书类型")
    @GetMapping("/queryChildBookType")
    @ResponseBody
    public ResponseResult queryChildBookType(String parent_code) {
        ResponseResult responseResult = iManagerBookService.getChildBookType(parent_code);
        return responseResult;
    }

    @ApiOperation(value = "获得父子图书类型键值对集合")
    @GetMapping("/getParentChildList")
    @ResponseBody
    public ResponseResult getParentChildList() {
        ResponseResult responseResult = iManagerBookService.getParentChildList();
        return responseResult;
    }


}
