package com.msw.server.business.controller;

import com.msw.server.business.service.IManageUserConfigService;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.common.param.UserConfigParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Api("用户设置模块接口")
@Slf4j
@Controller
@RequestMapping("/bus")
public class UserConfigController {

    @Autowired
    private IManageUserConfigService iManageUserConfigService;

    @ApiOperation("获得用户的个人设置")
    @GetMapping("/getUserConfig")
    @ResponseBody
    public ResponseResult getUserConfig(HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult result = iManageUserConfigService.getUserConfig(user_id);
        return result;

    }

    @ApiOperation("设置")
    @GetMapping("/setUserConfig")
    @ResponseBody
    public ResponseResult setUserConfig(UserConfigParam userConfig, HttpServletRequest request) {
        log.info("正在进行用户设置");
        String user_id = (String) request.getSession().getAttribute("user_id");
        userConfig.setUser_id(user_id);
        ResponseResult result = iManageUserConfigService.setUserConfig(userConfig);
        return result;

    }

}
