package com.msw.server.business.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.consts.SysConst;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;


@ApiModel(value = "BookHouse对象", description = "表book_house")
@Data
@ToString
@Accessors(chain = true)
public class BookHouse implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("书号")
    private String book_id;

    @ApiModelProperty("书名")
    private String book_name;

    @ApiModelProperty("书图片")
    private String book_img_url;

    @ApiModelProperty("作者")
    private String book_author;

    @ApiModelProperty("A类型")
    private String book_a_type;

    @ApiModelProperty("B类型")
    private String book_b_type;

    @ApiModelProperty("C类型")
    private String book_c_type;

    @ApiModelProperty("D类型")
    private String book_d_type;

    @ApiModelProperty("书的特色")
    private String book_feature;

    @ApiModelProperty("出版单位")
    private String publish_unit;

    @ApiModelProperty("出版时间")
    private Date publish_time;

    @ApiModelProperty("价格")
    private Double book_price;

    @ApiModelProperty("页数")
    private long book_pages;

    @ApiModelProperty("国家")
    private String book_country;

    @ApiModelProperty("试用年龄")
    private int trial_age;

    @ApiModelProperty("收藏数量")
    private long collect_num;

    @ApiModelProperty("推荐数量")
    private long recommend_num;

    @ApiModelProperty("平均分")
    private String avg_score;

    @ApiModelProperty("阅读数量")
    private long read_num;

    @ApiModelProperty("上架时间")
    private Date put_time;




}