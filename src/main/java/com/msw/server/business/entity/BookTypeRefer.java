package com.msw.server.business.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "BookTypeRefer对象", description = "表book_type_refer")
@Data
@ToString
public class BookTypeRefer implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("code")
    private String book_type_code;

    @ApiModelProperty("描述")
    private String book_type_des;

    @ApiModelProperty("父级code")
    private String parent_code;

    @ApiModelProperty("创建时间")
    private Date b_create_time;
}
