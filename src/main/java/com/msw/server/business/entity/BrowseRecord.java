package com.msw.server.business.entity;

import java.io.Serializable;
import java.util.Date;


import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@ClassAnnotation("一个用户多次浏览同一本书时只生成一条该书的浏览记录")
@ApiModel(value = "BrowseRecord对象", description = "表browse_record")
@Data
@ToString
@Accessors(chain = true)
public class BrowseRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增id")
    private long id;

    @ApiModelProperty("用户id")
    private String user_id;

    @ApiModelProperty("书号")
    private String book_id;

    @ApiModelProperty("书名")
    private String book_name;

    @ApiModelProperty("作者")
    private String book_author;

    @ApiModelProperty("类型")
    private String book_type;

    @ApiModelProperty("创建时间")
    private Date create_time;

    @ApiModelProperty("浏览次数")
    private long browse_num;


}