package com.msw.server.business.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "CountryRefer对象", description = "表country_refer")
@Data
@ToString
public class CountryRefer implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("code")
    private String country_code;

    @ApiModelProperty("描述")
    private String country_des;

    @ApiModelProperty("创建时间")
    private Date c_create_time;
}
