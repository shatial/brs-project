package com.msw.server.business.entity;

import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
@ClassAnnotation("用户对同一本书只能生成一条推荐记录")
@ApiModel(value = "RecommendRecord对象", description = "recommend_record表")
@Data
@ToString
@Accessors(chain = true)
public class RecommendRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增id")
    private long id;

    @ApiModelProperty("用户id")
    private String user_id;


    @ApiModelProperty("书号")
    private String book_id;


    @ApiModelProperty("书名")
    private String book_name;


    @ApiModelProperty("作者")
    private String book_author;


    @ApiModelProperty("类型")
    private String book_type;


    @ApiModelProperty("创建时间")
    private Date create_time;


}
