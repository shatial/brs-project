package com.msw.server.business.entity;

import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import sun.reflect.CallerSensitive;

import java.io.Serializable;
import java.util.Date;
@ClassAnnotation("用户可生成对于一本书的回收记录")
@ApiModel(value = "RecycleBin对象", description = "recyclebin表")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class RecycleBin implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增id")
    private long id;

    @ApiModelProperty("用户id")
    private String user_id;


    @ApiModelProperty("书号")
    private String book_id;


    @ApiModelProperty("书名")
    private String book_name;


    @ApiModelProperty("作者")
    private String book_author;


    @ApiModelProperty("类型")
    private String book_type;


    @ApiModelProperty("创建时间")
    private Date create_time;




}
