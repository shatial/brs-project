package com.msw.server.business.entity;

import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.awt.*;
import java.io.Serializable;
import java.util.Date;
@ClassAnnotation("用户只能对一本书生成一条评分记录")
@ApiModel(value = "ScoreRecord", description = "表score_record")
@Data
@ToString
@Accessors(chain = true)
public class ScoreRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增id")
    private long id;

    @ApiModelProperty("用户id")
    private String user_id;

    @ApiModelProperty("书号")
    private String book_id;

    @ApiModelProperty("书名")
    private String book_name;

    @ApiModelProperty("作者")
    private String book_author;

    @ApiModelProperty("类型")
    private String book_type;

    @ApiModelProperty("创建时间")
    private Date create_time;

    @ApiModelProperty("分数")
    private int score;

}
