package com.msw.server.business.entity;

import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@ClassAnnotation("用户可对同一本书生成多条上传记录")
@ApiModel(value = "UploadRecord对象", description = "upload_record表")
@Data
@ToString
@Accessors(chain = true)
public class UploadRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增id")
    private long id;

    @ApiModelProperty("账号")
    private String user_id;

    @ApiModelProperty("书名")
    private String book_name;

    @ApiModelProperty("作者")
    private String book_author;

    @ApiModelProperty("A类型")
    private String book_a_type;

    @ApiModelProperty("B类型")
    private String book_b_type;

    @ApiModelProperty("C类型")
    private String book_c_type;

    @ApiModelProperty("D类型")
    private String book_d_type;

    @ApiModelProperty("书的特色")
    private String book_feature;

    @ApiModelProperty("出版单位")
    private String publish_unit;

    @ApiModelProperty("出版时间")
    private Date publish_time;

    @ApiModelProperty("价格")
    private Double book_price;

    @ApiModelProperty("页数")
    private long book_pages;

    @ApiModelProperty("国家")
    private String book_country;

    @ApiModelProperty("试用年龄")
    private int trial_age;

    private int status;

    @ApiModelProperty("上架时间")
    private Date create_time;


}
