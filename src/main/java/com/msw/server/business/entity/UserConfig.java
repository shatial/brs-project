package com.msw.server.business.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "UserConfig对象", description = "user_config表")
@Data
@ToString
public class UserConfig implements Serializable {
    @ApiModelProperty("账号")
    private String user_id;

    @ApiModelProperty("浏览模式")
    private String browse_mode;

    @ApiModelProperty("评论模式")
    private String comment_mode;

    @ApiModelProperty("邮件模式")
    private String mail_receive_mode;

    @ApiModelProperty("上传记录模式")
    private String upload_record_mode;

    @ApiModelProperty("搜索模式")
    private String search_mode;

    @ApiModelProperty("专业书检索模式")
    private String PSearchMode;

    @ApiModelProperty("工具书检索模式")
    private String RSearchMode;

    @ApiModelProperty("儿童读物检索模式")
    private String CSearchMode;

    @ApiModelProperty("文学作品检索模式")
    private String LSearchMode;

    @ApiModelProperty("文献资料检索模式")
    private String DSearchMode;

    @ApiModelProperty("综合图书检索模式")
    private String MSearchMode;

    @ApiModelProperty("推荐结果排序模式")
    private String sort_mode;

    @ApiModelProperty("推荐模式")
    private String recommend_mode;

    @ApiModelProperty("推荐结果评分")
    private String result_score;

    @ApiModelProperty("创建时间")
    private Date create_time;

}
