package com.msw.server.business.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.io.Serializable;

@ApiModel(value = "UserInfo对象", description = "UserInfo表")
@Data
@ToString
@Accessors(chain = true)
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户账号")
    private String user_id;
   
    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("年龄")
    private String sex;

    @ApiModelProperty("用户账号")
    private int age;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("电子邮件")
    private String e_mail;

    @ApiModelProperty("省份")
    private String province;

    @ApiModelProperty("城市")
    private String city;

    @ApiModelProperty("地区")
    private String county;

    @ApiModelProperty("职业")
    private String work_a_type;

    @ApiModelProperty("职业")
    private String work_b_type;

    @ApiModelProperty("职业")
    private String work_c_type;

    @ApiModelProperty("爱好")
    private String hobby_des;
}
