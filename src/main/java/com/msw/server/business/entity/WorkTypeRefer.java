package com.msw.server.business.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "WorkTypeRefer对象", description = "表work_type_refer")
@Data
@ToString
public class WorkTypeRefer implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("职业code")
    private String work_code;

    @ApiModelProperty("职业描述")
    private String work_des;

    @ApiModelProperty("父级code")
    private String parent_code;

    @ApiModelProperty("创建时间")
    private Date w_create_time;
}
