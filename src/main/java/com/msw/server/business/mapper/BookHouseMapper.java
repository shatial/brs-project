package com.msw.server.business.mapper;

import com.msw.server.business.entity.BookHouse;
import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface BookHouseMapper {

    @MethodAnnotation("增加图书")
    public int insertBook(BookHouse bookHouse);

    @MethodAnnotation("通过书名和作者名查询图书")
    public BookHouse selectBook(String book_name, String book_author);

    @MethodAnnotation("查询书的数量")
    public int selectCount(BookHouse bookHouse);

    @MethodAnnotation("查询书的集合")
    public List<BookHouse> selectBooks(BookHouse bookHouse);

    @MethodAnnotation("模糊查询(姓名/作者模式)")
    public List<BookHouse> selectLikeDefault(String book_name, String book_author, String book_a_type);

    @MethodAnnotation("模糊查询(关键字模式)")
    public List<BookHouse> selectLikeDesc(String book_a_type, String desc);

    public List<BookHouse> selectLike(String book_name, String book_author, String book_d_type,String book_id);

    @MethodAnnotation("通过书号查询书")
    public BookHouse selectBookByBookId(String book_id);

    @MethodAnnotation("查D书的最大数量的集合")
    public List<BookHouse> selectMaxTypeNumBooks(String bookNumOpt, String book_d_type);

    @MethodAnnotation("查询书的集合按数量排序")
    public List<BookHouse> selectOrderBooks(String bookSortOpt, String book_a_type,String book_b_type, String book_c_type, String book_d_type,String avg_score);

    @MethodAnnotation("查询某类型书的最大的id")
    public String selectMaxId(String book_a_type);

    @MethodAnnotation("对书的数量加一")
    public int updatePluNum(String bookNumOpt, String book_id);

    @MethodAnnotation("对书的数量减一")
    public int updateSubNuM(String bookNumOpt, String book_id);

    @MethodAnnotation("更新评分")
    public int updateAvgScore(String book_id, String avg_score);


}
