package com.msw.server.business.mapper;

import com.msw.server.business.entity.BookTypeRefer;
import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookTypeReferMapper {

    @MethodAnnotation("通过标识查询描述")
    public String selectBookTypeDes(String book_type_code);

    @MethodAnnotation("通过描述查询标识")
    public String selectBookTypeCode(String book_type_des);

    @MethodAnnotation("通过父级标识查询子级的集合")
    public List<BookTypeRefer> selectChildBookType(String parent_code);

    @MethodAnnotation("查询父级的集合")
    public List<BookTypeRefer> selectParentBookType();
}
