package com.msw.server.business.mapper;

import com.msw.server.business.entity.BrowseRecord;
import com.msw.server.business.vo.RecordVO;
import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface BrowseRecordMapper {
    @MethodAnnotation("增加一条浏览记录")
    public int insertBrowseRecord(BrowseRecord browseRecord);

    @MethodAnnotation("按条件查询记录的数量")
    public int selectCount(BrowseRecord browseRecord);

    @MethodAnnotation("查询某人昨日的浏览记录集合")
    public List<BrowseRecord> selectYesBrowseRecords(String user_id);

    @MethodAnnotation("查询某人最新的浏览记录集合")
    public BrowseRecord selectLatestBrowseRecords(String user_id);

    @MethodAnnotation("查询某人在某个时间段浏览记录的数量")
    public int selectBrowseRecordByTypeAndTime(String book_type, String start, String end, String user_id);

    @MethodAnnotation("通过条件查询浏览记录的集合")
    public List<RecordVO> selectBrowseRecords(BrowseRecord browseRecord);

    @MethodAnnotation("查询某人对某本书的浏览记录")
    public BrowseRecord selectBrowseRecord(String book_id, String user_id);

    @MethodAnnotation("查询某人浏览记录的类型")
    public List<String> selectBrowseType(String user_id);

    @MethodAnnotation("根据主键删除一条浏览记录")
    public int deleteBrowseRecord(long id);

    @MethodAnnotation("根据条件跟新浏览记录")
    public int updateBrowseRecord(BrowseRecord browseRecord);


}
