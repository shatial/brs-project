package com.msw.server.business.mapper;

import com.msw.server.business.entity.CollectRecord;
import com.msw.server.business.vo.RecordVO;
import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.param.KeyValue;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface CollectRecordMapper {

    @MethodAnnotation("增加一条收藏记录")
    public int insertCollectRecord(CollectRecord collectRecord);

    @MethodAnnotation("根据条件查询收藏记录的集合")
    public List<RecordVO> selectCollectRecords(CollectRecord collectRecord);

    @MethodAnnotation("查询某人对某本书的收藏记录")
    public CollectRecord selectCollectRecord(String book_id, String user_id);

    @MethodAnnotation("通过主键查询一条收藏记录")
    public CollectRecord selectACollectRecord(long id);

    @MethodAnnotation("查询某人收藏记录的类型以及该类型的数量")
    public List<KeyValue>selectCollectTypeCount(String user_id);

    @MethodAnnotation("根据主键删除一条收藏记录")
    public int deleteCollectRecord(long id);

    @MethodAnnotation("根据条件查询收藏记录的集合")
    public int selectCount(CollectRecord collectRecord);
}
