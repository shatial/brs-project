package com.msw.server.business.mapper;

import com.msw.server.business.entity.CommentRecord;
import com.msw.server.business.vo.RecordVO;
import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface CommentRecordMapper {
    @MethodAnnotation("增加一条评论记录")
    public int insertCommentRecord(CommentRecord commentRecord);

    @MethodAnnotation("根据条件查询评论记录的数量")
    public int selectCount(CommentRecord commentRecord);

    @MethodAnnotation("根据条件查询评论记录的集合")
    public List<RecordVO> selectCommentRecords(CommentRecord commentRecord);

    @MethodAnnotation("查询某人对某本书的评论记录")
    public CommentRecord selectCommentRecord(String book_id, String user_id);

    @MethodAnnotation("查询某本书的评论记录的集合")
    public List<CommentRecord> selectCommentRecordByBookId(String book_id,String user_id);

    @MethodAnnotation("通过主键跟新评论记录的评论")
    public int updateCommentRecord(long id, String content);

    @MethodAnnotation("根据主键删除某条评论记录")
    public int deleteCommentRecord(long id);


}
