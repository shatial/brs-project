package com.msw.server.business.mapper;

import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CountryReferMapper {
    @MethodAnnotation("根据标识查询描述")
    public String selectCountryDes(String country_code);
}
