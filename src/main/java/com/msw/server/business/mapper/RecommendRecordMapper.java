package com.msw.server.business.mapper;

import com.msw.server.business.entity.RecommendRecord;
import com.msw.server.business.vo.RecordVO;
import com.msw.server.common.annotation.MethodAnnotation;
import java.util.List;

public interface RecommendRecordMapper {
    @MethodAnnotation("增加一条推荐记录")
    public int insertRecommendRecord(RecommendRecord recommendRecord);

    @MethodAnnotation("根据条件查询推荐记录的集合")
    public List<RecordVO> selectRecommendRecords(RecommendRecord recommendRecord);

    @MethodAnnotation("根据条件查询推荐记录的数量")
    public int selectCount(RecommendRecord recommendRecord);

    @MethodAnnotation("查询某人对某本书的推荐记录")
    public RecommendRecord selectRecommendRecord(String book_id, String user_id);

    @MethodAnnotation("根据主键查询一条推荐记录")
    public RecommendRecord selectRecommendRecordById(long id);

    @MethodAnnotation("根据主键删除一条推荐记录")
    public int deleteRecommendRecord(long id);

}
