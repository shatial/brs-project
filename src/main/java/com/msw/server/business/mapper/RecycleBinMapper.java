package com.msw.server.business.mapper;

import com.msw.server.business.entity.RecycleBin;
import com.msw.server.business.vo.RecordVO;
import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RecycleBinMapper {
    @MethodAnnotation("增加一条回收站记录")
    public int insertRecycleBin(RecycleBin recycleBin);

    @MethodAnnotation("根据条件查询回收记录的集合")
    public List<RecordVO> selectRecycleBins(RecycleBin recycleBin);

    @MethodAnnotation("根据条件查询回收记录的数量")
    public int selectCount(RecycleBin recycleBin);

    @MethodAnnotation("根据主键查询一条回收站记录")
    public RecycleBin selectRecycleBin(long id);

    @MethodAnnotation("根据主键删除一条回收站记录 ")
    public int deleteRecycleBin(long id);


}
