package com.msw.server.business.mapper;

import com.msw.server.business.entity.ScoreRecord;
import com.msw.server.business.vo.RecordVO;
import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.param.KeyValue;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface ScoreRecordMapper {
    @MethodAnnotation("增加一条评分记录")
    public int insertScoreRecord(ScoreRecord scoreRecord);

    @MethodAnnotation("查询所有评分记录")
    public List<ScoreRecord> selectAllScoreRecords();

    public List<ScoreRecord> selectScoreRecordByUserId(String user_id);

    @MethodAnnotation("根据条件查询评分记录的集合")
    public List<RecordVO> selectScoreRecords(ScoreRecord scoreRecord);

    @MethodAnnotation("根据条件查询评分记录")
    public ScoreRecord selectScoreRecordById(long id);

    @MethodAnnotation("查询某人对某本书的评分记录")
    public ScoreRecord selectScoreRecord(String book_id, String user_id);

    @MethodAnnotation("查询某本书评分记录的集合")
    public List<ScoreRecord> selectScoreRecordsByBookId(String book_id);

    @MethodAnnotation("根据条件查询评分记录的数量")
    public int selectCount(ScoreRecord scoreRecord);

    @MethodAnnotation("查询某本书的平均分")
    public float selectAvgScoreRecord(String book_id);

    public List<KeyValue> selectTypeScore(String user_id);

    @MethodAnnotation("跟新某条评分记录的分数")
    public int updateScoreRecord(long id, int score);

    @MethodAnnotation("根据主键删除一条评分记录")
    public int deleteScoreRecord(long id);


}
