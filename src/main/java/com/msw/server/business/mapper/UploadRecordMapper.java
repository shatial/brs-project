package com.msw.server.business.mapper;

import com.msw.server.business.entity.UploadRecord;
import com.msw.server.business.vo.RecordVO;
import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface UploadRecordMapper {
    @MethodAnnotation("增加一条上传记录")
    public int insertUploadRecord(UploadRecord uploadRecord);

    public int  updateUploadRecord(long id);

    public UploadRecord selectUploadRecordById(long id);

    @MethodAnnotation("根据条件查询上传记录的集合")
    public List<RecordVO> selectUploadRecords(UploadRecord uploadRecord);

    @MethodAnnotation("根据条件查询上传记录的数量")
    public int selectCount(UploadRecord uploadRecord);

    @MethodAnnotation("根据主键删除一条上传记录")
    public int deleteUploadRecord(long id);


}
