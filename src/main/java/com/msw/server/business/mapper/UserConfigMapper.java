package com.msw.server.business.mapper;

import com.msw.server.business.entity.UserConfig;

import com.msw.server.common.annotation.MethodAnnotation;

public interface UserConfigMapper {

    @MethodAnnotation("增加用户配置")
    public int insertUserConfig(UserConfig userConfig);

    @MethodAnnotation("查询某人的配置集合")
    public UserConfig selectUserConfigByUserId(String user_id);

    @MethodAnnotation("更新某人的某个配置的值")
    public int updateUserConfig(UserConfig userConfig);
}
