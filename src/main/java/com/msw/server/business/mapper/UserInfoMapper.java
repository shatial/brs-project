package com.msw.server.business.mapper;

import com.msw.server.business.entity.UserInfo;
import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserInfoMapper {
    @MethodAnnotation("增加一条用户信息")
    public int insertUserInfo(UserInfo userInfo);

    @MethodAnnotation("根据账号查询用户信息")
    public UserInfo selectUserInfo(String user_id);

    @MethodAnnotation("根据邮箱号查询用户信息")
    public UserInfo selectUserInfoByMail(String e_mail);

    @MethodAnnotation("跟新用户信息")
    public int updateUserInfo(UserInfo userInfo);


}
