package com.msw.server.business.mapper;

import com.msw.server.business.entity.WorkTypeRefer;
import com.msw.server.common.annotation.MethodAnnotation;
import java.util.List;

public interface WorkTypeReferMapper {
    @MethodAnnotation("根据父级标识查询子级集合")
    public List<WorkTypeRefer> selectChildWorkType(String parent_code);

    @MethodAnnotation("查询所有父级类型的集合")
    public List<WorkTypeRefer> selectParentWorkType();

    @MethodAnnotation("根据code查询描述")
    public String selectWorkDescByCode(String work_code);
}
