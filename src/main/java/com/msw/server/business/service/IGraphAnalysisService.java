package com.msw.server.business.service;

import com.msw.server.common.param.ResponseResult;
import java.io.IOException;

public interface IGraphAnalysisService {

    public ResponseResult getBrowseWeekGraph(String user_id) throws Exception;

    public ResponseResult getCollectLikeGraph(String user_id);

    public ResponseResult sortCollectType(String user_id) throws IOException;

    public ResponseResult getTypeBookRecommend(String user_id) throws IOException;

}
