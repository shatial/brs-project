package com.msw.server.business.service;

import com.msw.server.business.entity.*;
import com.msw.server.common.param.ResponseResult;

public interface IManageRecordService {

    //浏览记录接口
    public ResponseResult createBrowseRecord(String book_id, String user_id);

    public ResponseResult getBrowseRecord(String book_id, String user_id);

    public ResponseResult getBrowseRecords(int page, int limit, BrowseRecord browseRecord);

    public ResponseResult deleteBrowseRecords(long[] ids);

    public ResponseResult getYesBrowseRecords(String user_id);

    public ResponseResult getMyBrowseType(String user_id);

    public ResponseResult getCountBrowseRecords(String book_type, String start, String end, String user_id);

    public ResponseResult getUserLatestBrowseRecords(String user_id);

    //收藏记录接口
    public ResponseResult createCollectRecord(String book_id, String user_id);

    public ResponseResult getCollectTypeCount(String user_id);

    public ResponseResult getMaxCountType(String user_id);

    public ResponseResult getCollectRecords(int page, int limit, CollectRecord collectRecord);

    public ResponseResult deleteCollectRecords(long[] ids,int opt);

    //回收站记录接口
    public ResponseResult getRecycleBins(int page, int limit, RecycleBin recycleBin);

    public ResponseResult deleteRecycleBins(long[] ids, int opt, String user_id);

    //评论记录接口
    public ResponseResult createCommentRecord(String book_id, String user_id, String content);

    public ResponseResult getCommentRecords(int page, int limit, CommentRecord commentRecord);

    public ResponseResult getCommentRecordsByBookId(String book_id,String user_id);

    public ResponseResult deleteCommentRecords(long[] ids);

    public ResponseResult modifyContent(long id, String content);

    //评分记录接口
    public ResponseResult createScoreRecord(String book_id,String user_id,int score);

    public ResponseResult createScoreCsvFile();

    public ResponseResult getScoreRecord(String user_id);

    public ResponseResult getScoreRecords(int page, int limit, ScoreRecord scoreRecord);

    public ResponseResult getScoreRecordsByBookId(String book_id);
    
    public ResponseResult getMyScoreToBook(String user_id,String book_id);

    public ResponseResult getMaxAvgScoreType(String user_id);

    public ResponseResult deleteScoreRecords(long[] ids);

    public ResponseResult modifyScore(long id, int score);

    //上传记录接口
    public ResponseResult createUploadRecord(UploadRecord uploadRecord);

    public ResponseResult passOrNoPass(long id,int check,String user_id);

    public ResponseResult getUploadRecords(int page, int limit, UploadRecord uploadRecord);

    public ResponseResult deleteUploadRecords(long[] ids);

    //推荐记录接口
    public ResponseResult createRecommendRecord(String book_id, String user_id);

    public ResponseResult getRecommendRecords(int page, int limit, RecommendRecord recommendRecord);

    public ResponseResult deleteRecommendRecords(long[] ids);

    public ResponseResult revokeRecommend(long id,String book_id, String user_id);


}
