package com.msw.server.business.service;


import com.msw.server.common.param.ResponseResult;
import com.msw.server.common.param.UserConfigParam;

public interface IManageUserConfigService {

    public ResponseResult createDefaultUserConfig(String user_id);

    public ResponseResult getUserConfig(String user_id);

    public ResponseResult setUserConfig(UserConfigParam userConfigParam);

}
