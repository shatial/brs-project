package com.msw.server.business.service;

import com.msw.server.business.entity.UserInfo;
import com.msw.server.common.param.ResponseResult;

public interface IManageUserService {

    public ResponseResult getUserInfo(String user_id);

    public ResponseResult perfectUserInfo(UserInfo userInfo);

    public ResponseResult getUserInfoByMail(String e_mail);

    public ResponseResult getParentWorkType();

    public ResponseResult getChildWorkType(String parent_code);

}
