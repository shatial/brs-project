package com.msw.server.business.service;

import com.msw.server.business.entity.BookHouse;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.common.param.SearchParam;

public interface IManagerBookService {

    public ResponseResult addABook(BookHouse bookHouse);

    public ResponseResult getTypeBooks(int page, int limit, BookHouse bookHouse, String user_id);

    public ResponseResult searchBooks(SearchParam searchParam);

    public ResponseResult querySimilarBooks(SearchParam searchParam);

    public ResponseResult getBooksByBookId(String book_id);

    public ResponseResult getBookDetails(String book_id, String user_id) ;

    public ResponseResult plusOne(String bookNumOpt, String book_id);

    public ResponseResult subOne(String bookNumOpt, String book_id);

    public ResponseResult getBookTypeDes(String book_type_code);

    public ResponseResult getBookTypeCode(String book_type_des);

    public ResponseResult getParentBookType();

    public ResponseResult getChildBookType(String parent_code);

    public ResponseResult getParentChildList();

    public ResponseResult getNumSoreBooks(String bookSortOpt, BookHouse bookHouse);

    public ResponseResult getMaxTypeNumBook(String bookNumOpt, String book_d_type);

    public ResponseResult modifyScore(String book_id, String avg_score);

}
