package com.msw.server.business.service;

import com.msw.server.business.entity.BookHouse;
import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.param.RecommendParam;
import com.msw.server.common.param.ResponseResult;

import java.util.List;

@ClassAnnotation("推荐接口")
public interface IRecommendBooksService {

    @MethodAnnotation("使用推荐接口")
    public ResponseResult useRecommendApi(String user_id) throws Exception;

    @MethodAnnotation("通过用户的职业进行推荐")
    public ResponseResult recommendBooksByUserWork(RecommendParam recommendParam);

    @MethodAnnotation("推荐热门图书")
    public ResponseResult recommendBooksByLatest(RecommendParam recommendParam);

    @MethodAnnotation("通过评分记录推荐图书")
    public ResponseResult recommendBooksByScoreRecord(RecommendParam recommendParam) throws Exception;

    @MethodAnnotation("用过浏览记录推荐图书")
    public ResponseResult recommendBooksByBrowseRecord(RecommendParam recommendParam);

    @MethodAnnotation("通过收藏记录推荐图书")
    public ResponseResult recommendBooksByCollectRecord(RecommendParam recommendParam);

    @MethodAnnotation("推荐相似图书")
    public ResponseResult recommendSimilarBooks(RecommendParam recommendParam);

    @MethodAnnotation("去除用户已经阅读的图书")
    public List<BookHouse> getUserNoReadBooksInRecommendBooks(String user_id, List<BookHouse> bookLists);

}
