package com.msw.server.business.service.impl;


import com.msw.server.business.entity.BookHouse;
import com.msw.server.business.service.IGraphAnalysisService;
import com.msw.server.business.service.IManageRecordService;
import com.msw.server.business.service.IManagerBookService;
import com.msw.server.business.vo.LineEChartsVO;
import com.msw.server.business.vo.RoseEChartsVO;
import com.msw.server.common.consts.BusConst;
import com.msw.server.common.param.KeyValue;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.common.utils.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class IGraphAnalysisServiceImpl implements IGraphAnalysisService {
    @Autowired
    private IManageRecordService iManageRecordService;
    @Autowired
    private IManagerBookService iManagerBookService;

    @Override
    public ResponseResult getBrowseWeekGraph(String user_id) throws Exception {
        //根据当前日期调用工具类查询一周的日期
        List<String> weekTimes = DateTimeUtil.getWeekTime();
        String nextDay = DateTimeUtil.getNextDay(weekTimes.get(weekTimes.size() - 1));
        weekTimes.add(nextDay);
        Map<String, List<Integer>> typeCount = new HashMap<>();
        List<String> bookTypes = (List<String>) iManageRecordService.getMyBrowseType(user_id).getData();
        for (String type : bookTypes) {
            List<Integer> counts = new ArrayList<>();
            for (int i = 0; i < 7; i++) {//循环遍历该类型的每天的浏览总量
                ResponseResult a1 = iManageRecordService.getCountBrowseRecords(type, weekTimes.get(i), weekTimes.get(i + 1), user_id);
                counts.add((int) a1.getCount());
            }
            typeCount.put(type, counts);
        }
        List<LineEChartsVO> lists = new ArrayList<>();
        for (String key : typeCount.keySet()) {//设置视图层VO
            LineEChartsVO lineEChartsVO = new LineEChartsVO();
            String bookParentTypeDes = (String) iManagerBookService.getBookTypeDes(key).getData();
            lineEChartsVO.setName(bookParentTypeDes);
            lineEChartsVO.setType("line");
            lineEChartsVO.setData(typeCount.get(key));
            lists.add(lineEChartsVO);
        }
        return ResponseResult.success(lists);
    }

    @Override
    public ResponseResult getCollectLikeGraph(String user_id) {
        List<KeyValue> typeCount = (List<KeyValue>) iManageRecordService.getCollectTypeCount(user_id).getData();
        List<RoseEChartsVO> lists = new ArrayList<>();
        for (KeyValue key : typeCount) {//设置视图层VO
            RoseEChartsVO roseEChartsVO = new RoseEChartsVO();
            String typeDes = (String) iManagerBookService.getBookTypeDes(key.getKey()).getData();
            roseEChartsVO.setName(typeDes);
            roseEChartsVO.setValue(key.getCount());
            lists.add(roseEChartsVO);
        }
        return ResponseResult.success(lists);
    }

    @Override
    public ResponseResult sortCollectType(String user_id) throws IOException {
        List<RoseEChartsVO> roseEChartsVOS = (List<RoseEChartsVO>) getCollectLikeGraph(user_id).getData();
        Collections.sort(roseEChartsVOS, new Comparator<RoseEChartsVO>() {//根据Value字段排序
            @Override
            public int compare(RoseEChartsVO o1, RoseEChartsVO o2) {
                if (o1.getValue() > o2.getValue()) {
                    return -1;
                }
                if (o1.getValue() == o2.getValue()) {
                    return 0;
                }
                return 1;
            }
        });
        return ResponseResult.success(roseEChartsVOS);
    }

    @Override
    public ResponseResult getTypeBookRecommend(String user_id) throws IOException {
        List<RoseEChartsVO> roseEChartsVOS = (List<RoseEChartsVO>) sortCollectType(user_id).getData();
        List<BookHouse> recommendBook = new ArrayList<>();
        for (int i = 0; i < roseEChartsVOS.size(); i++) {//取得每种类型的图书的最高的推荐量的集合
            String bookType = (String) iManagerBookService.getBookTypeCode(roseEChartsVOS.get(i).getName()).getData();
            List<BookHouse> books = (List<BookHouse>) iManagerBookService.getMaxTypeNumBook(BusConst.BUS_BOOK_RECOMMEND, bookType).getData();
            BookHouse book = new BookHouse();
            if (books == null || books.size() <= 0) {
                continue;
            } else {
                book.setBook_a_type(roseEChartsVOS.get(i).getName());//集合只取第一个元素
                book.setBook_name(books.get(0).getBook_name());
                book.setBook_author(books.get(0).getBook_author());
                recommendBook.add(book);
            }

        }
        return ResponseResult.success(recommendBook);
    }


}
