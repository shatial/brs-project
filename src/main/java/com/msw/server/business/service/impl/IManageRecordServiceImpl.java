package com.msw.server.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.msw.server.business.entity.*;
import com.msw.server.business.mapper.*;
import com.msw.server.business.service.IManageRecordService;

import com.msw.server.business.service.IManageUserConfigService;
import com.msw.server.business.service.IManageUserService;
import com.msw.server.business.service.IManagerBookService;


import com.msw.server.business.vo.RecordVO;

import com.msw.server.business.vo.RoseEChartsVO;
import com.msw.server.common.consts.BusConst;
import com.msw.server.common.consts.SysConst;
import com.msw.server.common.enums.StatusEnums;
import com.msw.server.common.param.*;
import com.msw.server.common.utils.BookRecordUtils;
import com.msw.server.common.utils.EmailUtils;
import com.msw.server.system.entity.MailLog;
import com.msw.server.system.service.ILogGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;

@Service
public class IManageRecordServiceImpl implements IManageRecordService {

    @Autowired
    private BrowseRecordMapper browseRecordMapper;
    @Autowired
    private CollectRecordMapper collectRecordMapper;
    @Autowired
    private RecycleBinMapper recycleBinMapper;
    @Autowired
    private CommentRecordMapper commentRecordMapper;
    @Autowired
    private ScoreRecordMapper scoreRecordMapper;
    @Autowired
    private UploadRecordMapper uploadRecordMapper;
    @Autowired
    private RecommendRecordMapper recommendRecordMapper;
    @Autowired
    private IManagerBookService iManagerBookService;
    @Autowired
    private IManageUserConfigService iManageUserConfigService;
    @Autowired
    private IManageUserService iManageUserService;
    @Autowired
    private ILogGenerateService iLogGenerateService;

    @Override
    public ResponseResult getBrowseRecords(int page, int limit, BrowseRecord browseRecord) {
        int count = browseRecordMapper.selectCount(browseRecord);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<RecordVO> browseRecords = browseRecordMapper.selectBrowseRecords(browseRecord);
            BookRecordUtils.formatRecordTime(browseRecords);
            PageInfo<RecordVO> roleBrowseRecords = new PageInfo<>(browseRecords);
            return ResponseResult.success(count, roleBrowseRecords.getList());
        }

    }

    @Override
    public ResponseResult deleteBrowseRecords(long[] ids) {
        for (int j = 0; j < ids.length; j++) {
            int i = browseRecordMapper.deleteBrowseRecord(ids[j]);
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult createBrowseRecord(String book_id, String user_id) {
        //读取用户配置
        UserConfig userConfig = (UserConfig) iManageUserConfigService.getUserConfig(user_id).getData();
        if (userConfig.getBrowse_mode().equals("false")) {
            return ResponseResult.success();
        } else {
            BrowseRecord existBrowseRecord = (BrowseRecord) getBrowseRecord(book_id, user_id).getData();
            if (existBrowseRecord != null) {
                int i = browseRecordMapper.updateBrowseRecord(existBrowseRecord);
                if (i == 1) {
                    return ResponseResult.success();
                } else {
                    return ResponseResult.error();
                }
            } else {
                RecordParam recordParam = new RecordParam();
                recordParam.setUser_id(user_id);
                recordParam.setBrowse_num(0);
                BookHouse bookHouse = (BookHouse) iManagerBookService.getBooksByBookId(book_id).getData();
                BrowseRecord record = (BrowseRecord) BookRecordUtils.getRecord(bookHouse, new BrowseRecord(), recordParam);
                int i = browseRecordMapper.insertBrowseRecord(record);
                iManagerBookService.plusOne(BusConst.BUS_BOOK_READ, book_id);
                if (i == 1) {
                    return ResponseResult.success();
                } else {
                    return ResponseResult.error();
                }
            }

        }

    }

    @Override
    public ResponseResult getBrowseRecord(String book_id, String user_id) {
        BrowseRecord browseRecord = browseRecordMapper.selectBrowseRecord(book_id, user_id);
        return ResponseResult.success(browseRecord);
    }

    @Override
    public ResponseResult getYesBrowseRecords(String user_id) {
        List<BrowseRecord> browseRecords = browseRecordMapper.selectYesBrowseRecords(user_id);
        return ResponseResult.success(browseRecords);
    }

    @Override
    public ResponseResult getMyBrowseType(String user_id) {
        List<String> bookTypes = browseRecordMapper.selectBrowseType(user_id);
        return ResponseResult.success(bookTypes);
    }

    @Override
    public ResponseResult getCountBrowseRecords(String book_type, String start, String end, String user_id) {
        int count = browseRecordMapper.selectBrowseRecordByTypeAndTime(book_type, start, end, user_id);
        return ResponseResult.success(count);
    }

    @Override
    public ResponseResult getUserLatestBrowseRecords(String user_id) {
        BrowseRecord browseRecords = browseRecordMapper.selectLatestBrowseRecords(user_id);
        return ResponseResult.success(browseRecords);
    }

    @Override
    public ResponseResult createCollectRecord(String book_id, String user_id) {
        CollectRecord existCollect = collectRecordMapper.selectCollectRecord(book_id, user_id);
        if (existCollect != null) {
            return ResponseResult.error(StatusEnums.BUS_BOOK_EXIST_COLLECT);
        } else {
            RecordParam recordParam = new RecordParam();
            recordParam.setUser_id(user_id);
            BookHouse bookHouse = (BookHouse) iManagerBookService.getBooksByBookId(book_id).getData();
            CollectRecord record = (CollectRecord) BookRecordUtils.getRecord(bookHouse, new CollectRecord(), recordParam);
            collectRecordMapper.insertCollectRecord(record);
            iManagerBookService.plusOne(BusConst.BUS_BOOK_COLLECT, book_id);
            return ResponseResult.success(StatusEnums.BUS_BOOK_COLLECT_SUCCESS);
        }

    }

    @Override
    public ResponseResult getCollectTypeCount(String user_id) {
        List<KeyValue> typeCount = collectRecordMapper.selectCollectTypeCount(user_id);
        return ResponseResult.success(typeCount);
    }

    @Override
    public ResponseResult getMaxCountType(String user_id) {
        List<KeyValue> typeCount = (List<KeyValue>) getCollectTypeCount(user_id).getData();
        if (typeCount == null || typeCount.size() <= 0) {
            return ResponseResult.success("", null);
        } else {
            BookRecordUtils.keyValueSort(typeCount, "count");
            return ResponseResult.success("", typeCount.get(0).getKey());
        }

    }

    @Override
    public ResponseResult getCollectRecords(int page, int limit, CollectRecord collectRecord) {
        int count = collectRecordMapper.selectCount(collectRecord);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<RecordVO> collectRecords = collectRecordMapper.selectCollectRecords(collectRecord);
            BookRecordUtils.formatRecordTime(collectRecords);
            PageInfo<RecordVO> roleCollectRecords = new PageInfo<>(collectRecords);
            return ResponseResult.success(count, roleCollectRecords.getList());
        }
    }

    @Override
    public ResponseResult deleteCollectRecords(long[] ids, int opt) {
        for (int j = 0; j < ids.length; j++) {
            CollectRecord collectRecord = collectRecordMapper.selectACollectRecord(ids[j]);
            //插入到回收站
            RecycleBin recycleBin = new RecycleBin(0, collectRecord.getUser_id(),
                    collectRecord.getBook_id(), collectRecord.getBook_name(),
                    collectRecord.getBook_author(),
                    collectRecord.getBook_type(), collectRecord.getCreate_time()
            );
            if (opt == 2) {
                iManagerBookService.subOne(BusConst.BUS_BOOK_COLLECT, collectRecord.getBook_id());
            } else {
                int k = recycleBinMapper.insertRecycleBin(recycleBin);
            }
            int i = collectRecordMapper.deleteCollectRecord(ids[j]);
        }
        return ResponseResult.success();
    }


    @Override
    public ResponseResult getRecycleBins(int page, int limit, RecycleBin recycleBin) {
        int count = recycleBinMapper.selectCount(recycleBin);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<RecordVO> recycleBins = recycleBinMapper.selectRecycleBins(recycleBin);
            BookRecordUtils.formatRecordTime(recycleBins);
            PageInfo<RecordVO> roleRecycleBins = new PageInfo<>(recycleBins);
            return ResponseResult.success(count, roleRecycleBins.getList());
        }
    }

    @Override
    public ResponseResult deleteRecycleBins(long[] ids, int opt, String user_id) {
        if (opt == 1) {//删除
            for (int j = 0; j < ids.length; j++) {
                int i = recycleBinMapper.deleteRecycleBin(ids[j]);
            }
            return ResponseResult.success();
        } else {
            RecycleBin recycleBin = recycleBinMapper.selectRecycleBin(ids[0]);
            CollectRecord existCollectBook = collectRecordMapper.selectCollectRecord(recycleBin.getBook_id(), user_id);
            if (existCollectBook != null) {
                int i = recycleBinMapper.deleteRecycleBin(ids[0]);
                return ResponseResult.success(StatusEnums.BUS_BOOK_Exit);
            } else {//恢复
                CollectRecord collectRecord = new CollectRecord();
                collectRecord.setUser_id(recycleBin.getUser_id()).setBook_id(recycleBin.getBook_id())
                        .setBook_name(recycleBin.getBook_name()).setBook_author(recycleBin.getBook_author())
                        .setBook_type(recycleBin.getBook_type()).setCreate_time(recycleBin.getCreate_time());
                collectRecordMapper.insertCollectRecord(collectRecord);
                int i = recycleBinMapper.deleteRecycleBin(ids[0]);
                return ResponseResult.success();
            }


        }

    }

    @Override
    public ResponseResult createCommentRecord(String book_id, String user_id, String content) {
        CommentRecord existCommentRecord = commentRecordMapper.selectCommentRecord(book_id, user_id);
        if (existCommentRecord != null) {//如果存在就更新评论
            return modifyContent(existCommentRecord.getId(), content);
        } else {
            RecordParam recordParam = new RecordParam();
            recordParam.setUser_id(user_id).setContent(content);
            BookHouse bookHouses = (BookHouse) iManagerBookService.getBooksByBookId(book_id).getData();
            CommentRecord record = (CommentRecord) BookRecordUtils.getRecord(bookHouses, new CommentRecord(), recordParam);
            commentRecordMapper.insertCommentRecord(record);
            return ResponseResult.success(StatusEnums.BUS_BOOK_COMMENT_SUCCESS);
        }

    }

    @Override
    public ResponseResult getCommentRecords(int page, int limit, CommentRecord commentRecord) {
        int count = commentRecordMapper.selectCount(commentRecord);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<RecordVO> commentRecords = commentRecordMapper.selectCommentRecords(commentRecord);
            BookRecordUtils.formatRecordTime(commentRecords);
            PageInfo<RecordVO> roleCommentRecords = new PageInfo<>(commentRecords);
            return ResponseResult.success(count, roleCommentRecords.getList());
        }

    }

    @Override
    public ResponseResult getCommentRecordsByBookId(String book_id, String user_id) {
        UserConfig userConfig = (UserConfig) iManageUserConfigService.getUserConfig(user_id).getData();
        if (userConfig.getComment_mode().equals("true")) {
            user_id = null;
        }
        List<CommentRecord> commentRecords = commentRecordMapper.selectCommentRecordByBookId(book_id, user_id);
        return ResponseResult.success(commentRecords);
    }

    @Override
    public ResponseResult deleteCommentRecords(long[] ids) {
        for (int j = 0; j < ids.length; j++) {
            int i = commentRecordMapper.deleteCommentRecord(ids[j]);
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult modifyContent(long id, String content) {
        int i = commentRecordMapper.updateCommentRecord(id, content);
        if (i == 1) {
            return ResponseResult.success(StatusEnums.BUS_UPDATE_CONTENT_SUCCESS);
        } else {
            return ResponseResult.error(StatusEnums.BUS_UPDATE_CONTENT_ERROR);
        }

    }


    @Override
    public ResponseResult createScoreRecord(String book_id, String user_id, int score) {
        ScoreRecord existScoreRecord = scoreRecordMapper.selectScoreRecord(book_id, user_id);
        if (existScoreRecord != null) {
            return modifyScore(existScoreRecord.getId(), score);
        } else {
            RecordParam recordParam = new RecordParam();
            recordParam.setUser_id(user_id).setScore(score);
            BookHouse bookHouse = (BookHouse) iManagerBookService.getBooksByBookId(book_id).getData();
            ScoreRecord record = (ScoreRecord) BookRecordUtils.getRecord(bookHouse, new ScoreRecord(), recordParam);
            scoreRecordMapper.insertScoreRecord(record);
            createScoreCsvFile();
            float avgScore = scoreRecordMapper.selectAvgScoreRecord(book_id);
            iManagerBookService.modifyScore(book_id, String.valueOf(avgScore));
            return ResponseResult.success(StatusEnums.BUS_BOOK_SCORE_SUCCESS);
        }

    }

    @Override
    public ResponseResult createScoreCsvFile() {
        List<ScoreRecord> scoreRecords = scoreRecordMapper.selectAllScoreRecords();
        BookRecordUtils.createCSVFile(scoreRecords);
        return ResponseResult.success(scoreRecords);
    }

    @Override
    public ResponseResult getScoreRecord(String user_id) {
        List<ScoreRecord> scoreRecords = scoreRecordMapper.selectScoreRecordByUserId(user_id);
        return ResponseResult.success(scoreRecords);
    }

    @Override
    public ResponseResult getScoreRecords(int page, int limit, ScoreRecord scoreRecord) {
        int count = scoreRecordMapper.selectCount(scoreRecord);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<RecordVO> scoreRecords = scoreRecordMapper.selectScoreRecords(scoreRecord);
            BookRecordUtils.formatRecordTime(scoreRecords);
            PageInfo<RecordVO> roleScoreRecords = new PageInfo<>(scoreRecords);
            return ResponseResult.success(count, roleScoreRecords.getList());
        }
    }

    @Override
    public ResponseResult getScoreRecordsByBookId(String book_id) {
        List<ScoreRecord> scoreRecords = scoreRecordMapper.selectScoreRecordsByBookId(book_id);
        return ResponseResult.success(scoreRecords);
    }

    @Override
    public ResponseResult getMyScoreToBook(String user_id, String book_id) {
        ScoreRecord scoreRecord = scoreRecordMapper.selectScoreRecord(book_id, user_id);
        return ResponseResult.success(scoreRecord);
    }

    @Override
    public ResponseResult getMaxAvgScoreType(String user_id) {
        List<KeyValue> keyValues = scoreRecordMapper.selectTypeScore(user_id);
        BookRecordUtils.keyValueSort(keyValues, "avgScore");
        return ResponseResult.success("", keyValues.get(0).getKey());
    }

    @Override
    public ResponseResult deleteScoreRecords(long[] ids) {
        for (int j = 0; j < ids.length; j++) {
            int i = scoreRecordMapper.deleteScoreRecord(ids[j]);
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult modifyScore(long id, int score) {
        int i = scoreRecordMapper.updateScoreRecord(id, score);
        ScoreRecord scoreRecord = scoreRecordMapper.selectScoreRecordById(id);
        float avgScore = scoreRecordMapper.selectAvgScoreRecord(scoreRecord.getBook_id());
        iManagerBookService.modifyScore(scoreRecord.getBook_id(), String.valueOf(avgScore));
        if (i == 1) {
            createScoreCsvFile();
            return ResponseResult.success(StatusEnums.BUS_UPDATE_SCORE_SUCCESS);
        } else {
            return ResponseResult.error(StatusEnums.BUS_UPDATE_SCORE_ERROR);
        }

    }


    @Override
    public ResponseResult getUploadRecords(int page, int limit, UploadRecord uploadRecord) {
        int count = uploadRecordMapper.selectCount(uploadRecord);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<RecordVO> uploadRecords = uploadRecordMapper.selectUploadRecords(uploadRecord);
            BookRecordUtils.formatRecordTime(uploadRecords);
            PageInfo<RecordVO> roleUploadRecords = new PageInfo<>(uploadRecords);
            return ResponseResult.success(count, roleUploadRecords.getList());
        }
    }

    @Override
    public ResponseResult deleteUploadRecords(long[] ids) {
        for (int j = 0; j < ids.length; j++) {
            int i = uploadRecordMapper.deleteUploadRecord(ids[j]);
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult createUploadRecord(UploadRecord uploadRecord) {
        int i = uploadRecordMapper.insertUploadRecord(uploadRecord);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }

    }

    @Override
    public ResponseResult passOrNoPass(long id, int check, String user_id) {
        int i = uploadRecordMapper.updateUploadRecord(id);
        UploadRecord uploadRecord = uploadRecordMapper.selectUploadRecordById(id);
        UserInfo sender = (UserInfo) iManageUserService.getUserInfo(user_id).getData();
        UserInfo receiver = (UserInfo) iManageUserService.getUserInfo(uploadRecord.getUser_id()).getData();
        if (i == 1) {
            //创建邮件日志
            MailLog mailLog = new MailLog();
            if (check == 0) {
                mailLog = new MailLog(0, user_id, sender.getName(), sender.getE_mail(), receiver.getUser_id()
                        , receiver.getName(), receiver.getE_mail(), "审核结果", "图书：" + uploadRecord.getBook_name() + "审核不通过", "", SysConst.SYS_SUCCESS_STATUS, new Date());
            } else {
                BookHouse bookHouse = BookRecordUtils.getBookHouse(uploadRecord);
                iManagerBookService.addABook(bookHouse);
                mailLog = new MailLog(0, user_id, sender.getName(), sender.getE_mail(), receiver.getUser_id()
                        , receiver.getName(), receiver.getE_mail(), "审核结果", "图书：" + uploadRecord.getBook_name() + "审核通过", "", SysConst.SYS_SUCCESS_STATUS, new Date());
            }
            ResponseResult result = iLogGenerateService.createMailLog(mailLog);
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }
    }

    @Override
    public ResponseResult getRecommendRecords(int page, int limit, RecommendRecord recommendRecord) {
        int count = recommendRecordMapper.selectCount(recommendRecord);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<RecordVO> recommendRecords = recommendRecordMapper.selectRecommendRecords(recommendRecord);
            BookRecordUtils.formatRecordTime(recommendRecords);
            PageInfo<RecordVO> roleRecommendRecords = new PageInfo<>(recommendRecords);
            return ResponseResult.success(count, roleRecommendRecords.getList());
        }
    }

    @Override
    public ResponseResult deleteRecommendRecords(long[] ids) {
        for (int j = 0; j < ids.length; j++) {
            int i = recommendRecordMapper.deleteRecommendRecord(ids[j]);
        }
        return ResponseResult.success();
    }


    @Override
    public ResponseResult createRecommendRecord(String book_id, String user_id) {
        RecommendRecord existRecommendRecord = recommendRecordMapper.selectRecommendRecord(book_id, user_id);
        if (existRecommendRecord != null) {
            return ResponseResult.success();
        } else {
            RecordParam recordParam = new RecordParam();
            recordParam.setUser_id(user_id);
            BookHouse bookHouse = (BookHouse) iManagerBookService.getBooksByBookId(book_id).getData();
            RecommendRecord recommendRecord = (RecommendRecord) BookRecordUtils.getRecord(bookHouse, new RecommendRecord(), recordParam);
            recommendRecordMapper.insertRecommendRecord(recommendRecord);
            iManagerBookService.plusOne(BusConst.BUS_BOOK_RECOMMEND, book_id);
            return ResponseResult.success();
        }

    }

    @Override
    public ResponseResult revokeRecommend(long id, String book_id, String user_id) {
        int i = recommendRecordMapper.deleteRecommendRecord(id);
        iManagerBookService.subOne(BusConst.BUS_BOOK_RECOMMEND, book_id);
        return ResponseResult.success();
    }
}
