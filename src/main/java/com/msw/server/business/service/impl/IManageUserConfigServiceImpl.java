package com.msw.server.business.service.impl;

import com.msw.server.business.entity.UserConfig;
import com.msw.server.business.mapper.UserConfigMapper;
import com.msw.server.business.service.IManageUserConfigService;
import com.msw.server.common.enums.StatusEnums;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.common.param.UserConfigParam;
import com.msw.server.common.utils.BookRecordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IManageUserConfigServiceImpl implements IManageUserConfigService {
    @Autowired
    private UserConfigMapper userConfigMapper;

    @Override
    public ResponseResult createDefaultUserConfig(String user_id) {
        UserConfig userConfig = BookRecordUtils.setDefaultUserConfig(user_id);
        int i = userConfigMapper.insertUserConfig(userConfig);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }
    }

    @Override
    public ResponseResult getUserConfig(String user_id) {
        UserConfig userConfig = userConfigMapper.selectUserConfigByUserId(user_id);
        return ResponseResult.success(userConfig);
    }

    @Override
    public ResponseResult setUserConfig(UserConfigParam userConfigParam) {
        UserConfig userConfig = BookRecordUtils.setUserConfig(userConfigParam);
        int i = userConfigMapper.updateUserConfig(userConfig);
        if (i == 1) {
            return ResponseResult.success(StatusEnums.BUS_CONFIG_SUCCESS);
        } else {
            return ResponseResult.error(StatusEnums.BUS_CONFIG_ERROR);
        }
    }


}
