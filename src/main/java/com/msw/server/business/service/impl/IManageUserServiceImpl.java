package com.msw.server.business.service.impl;

import com.msw.server.business.entity.UserInfo;
import com.msw.server.business.entity.WorkTypeRefer;
import com.msw.server.business.mapper.UserInfoMapper;
import com.msw.server.business.mapper.WorkTypeReferMapper;
import com.msw.server.business.service.IManageUserService;
import com.msw.server.common.enums.StatusEnums;

import com.msw.server.common.param.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IManageUserServiceImpl implements IManageUserService {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private WorkTypeReferMapper workTypeReferMapper;

    @Override
    public ResponseResult perfectUserInfo(UserInfo userInfo) {
        ResponseResult isExitUserInfo = getUserInfo(userInfo.getUser_id());
        if (isExitUserInfo.getCode() == 200) {
            int i = userInfoMapper.updateUserInfo(userInfo);
            if (i == 1) {
                return ResponseResult.success(StatusEnums.BUS_PERFECT_INFO_SUCCESS);
            } else {
                return ResponseResult.error(StatusEnums.BUS_PERFECT_INFO_ERROR);
            }
        } else {
            int i = userInfoMapper.insertUserInfo(userInfo);
            if (i == 1) {
                return ResponseResult.success(StatusEnums.BUS_PERFECT_INFO_SUCCESS);
            } else {
                return ResponseResult.error(StatusEnums.BUS_PERFECT_INFO_ERROR);
            }
        }

    }

    @Override
    public ResponseResult getUserInfoByMail(String e_mail) {
        UserInfo userInfo = userInfoMapper.selectUserInfoByMail(e_mail);
        return ResponseResult.success(userInfo);
    }

    @Override
    public ResponseResult getParentWorkType() {
        List<WorkTypeRefer> workTypeList = workTypeReferMapper.selectParentWorkType();
        WorkTypeRefer[] workTypeArray = new WorkTypeRefer[workTypeList.size()];
        workTypeList.toArray(workTypeArray);
        return ResponseResult.success(workTypeArray);
    }

    @Override
    public ResponseResult getChildWorkType(String parent_code) {
        List<WorkTypeRefer> workTypeList = workTypeReferMapper.selectChildWorkType(parent_code);
        WorkTypeRefer[] workTypeArray = new WorkTypeRefer[workTypeList.size()];
        workTypeList.toArray(workTypeArray);
        return ResponseResult.success(workTypeArray);
    }

    @Override
    public ResponseResult getUserInfo(String user_id) {
        UserInfo userInfo = userInfoMapper.selectUserInfo(user_id);
        if (userInfo != null) {
            return ResponseResult.success(userInfo);
        } else {
            return ResponseResult.error();
        }

    }


}
