package com.msw.server.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msw.server.business.entity.*;

import com.msw.server.business.mapper.BookHouseMapper;
import com.msw.server.business.mapper.BookTypeReferMapper;
import com.msw.server.business.mapper.CountryReferMapper;
import com.msw.server.business.service.IManageRecordService;
import com.msw.server.business.service.IManageUserConfigService;
import com.msw.server.business.service.IManagerBookService;
import com.msw.server.business.service.IRecommendBooksService;
import com.msw.server.business.vo.BookDetailsVO;
import com.msw.server.business.vo.BookHouseVO;
import com.msw.server.business.vo.BookTypeMapVO;
import com.msw.server.common.enums.StatusEnums;
import com.msw.server.common.param.RecommendParam;
import com.msw.server.common.param.SearchParam;
import com.msw.server.common.utils.BookRecordUtils;
import com.msw.server.common.param.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class IManagerBookServiceImpl implements IManagerBookService {
    @Autowired
    private BookHouseMapper bookHouseMapper;
    @Autowired
    private IManageRecordService iManageRecordService;
    @Autowired
    private BookTypeReferMapper bookTypeReferMapper;
    @Autowired
    private CountryReferMapper countryReferMapper;
    @Autowired
    private IManageUserConfigService iManageUserConfigService;
    @Autowired
    private IRecommendBooksService iRecommendBooksService;


    @Override
    public ResponseResult addABook(BookHouse bookHouse) {
        BookHouse existBookHouse = bookHouseMapper.selectBook(bookHouse.getBook_name(), bookHouse.getBook_author());
        String maxId = bookHouseMapper.selectMaxId(bookHouse.getBook_a_type());
        BookRecordUtils.setDefaultBookValue(bookHouse, maxId);
        if (existBookHouse != null) {
            return ResponseResult.error(StatusEnums.BUS_BOOK_REPEAT);
        }
        int i = bookHouseMapper.insertBook(bookHouse);
        if (i == 1) {
            return ResponseResult.success(StatusEnums.BUS_BOOK_UPLOAD_SUCCESS);
        } else {
            return ResponseResult.error(StatusEnums.BUS_BOOK_UPLOAD_ERROR);
        }

    }

    @Override
    public ResponseResult getTypeBooks(int page, int limit, BookHouse bookHouse, String user_id) {
        UserConfig userConfig = (UserConfig) iManageUserConfigService.getUserConfig(user_id).getData();
        BookRecordUtils.setConfigBookTypes(bookHouse, userConfig);
        String bookSortOpt = userConfig.getSort_mode();
        int count = bookHouseMapper.selectCount(bookHouse);
        if (page == 0 && limit == 0) {
            return ResponseResult.success(count);
        }
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<BookHouse> books = bookHouseMapper.selectBooks(bookHouse);
            PageInfo<BookHouse> roleBooks = new PageInfo<>(books);
            return ResponseResult.success(count, roleBooks.getList());
        }

    }


    @Override
    public ResponseResult searchBooks(SearchParam searchParam) {
        List<BookHouse> books = new ArrayList<>();
        if (searchParam.getDesc() != null) {
            books = bookHouseMapper.selectLikeDesc(searchParam.getBook_a_type(), searchParam.getDesc());
        } else {
            books = bookHouseMapper.selectLikeDefault(searchParam.getBook_name(), searchParam.getBook_author(), searchParam.getBook_a_type());
        }
        return ResponseResult.success(books);
    }

    @Override
    public ResponseResult querySimilarBooks(SearchParam searchParam) {
        List<BookHouse> books = bookHouseMapper.selectLike(searchParam.getBook_name(), searchParam.getBook_author(), searchParam.getBook_d_type(), searchParam.getBook_id());
        return ResponseResult.success(books);
    }

    @Override
    public ResponseResult getBooksByBookId(String book_id) {
        BookHouse book = bookHouseMapper.selectBookByBookId(book_id);
        return ResponseResult.success(book);
    }

    @Override
    public ResponseResult getBookDetails(String book_id, String user_id) {
        UserConfig userConfig = (UserConfig) iManageUserConfigService.getUserConfig(user_id).getData();
        BookDetailsVO bookDetailsVO = new BookDetailsVO();
        BookHouseVO bookHouseVO = new BookHouseVO();
        BookHouse bookHouse = (BookHouse) getBooksByBookId(book_id).getData();
        //获取该书评论列表
        List<CommentRecord> commentRecords = (List<CommentRecord>) iManageRecordService.getCommentRecordsByBookId(book_id, user_id).getData();
        //获取该书评分列表
        List<ScoreRecord> scoreRecords = (List<ScoreRecord>) iManageRecordService.getScoreRecordsByBookId(book_id).getData();
        RecommendParam recommendParam = new RecommendParam();
        recommendParam.setUser_id(user_id);
        recommendParam.setBook_id(book_id);
        recommendParam.setBookSortOpt(userConfig.getSort_mode());
        //获取相似图书
        List<BookHouse> recommendBooks = (List<BookHouse>) iRecommendBooksService.recommendSimilarBooks(recommendParam).getData();
        String bookTypeDes = bookTypeReferMapper.selectBookTypeDes(bookHouse.getBook_a_type());
        String country_des = countryReferMapper.selectCountryDes(bookHouse.getBook_country());
        ScoreRecord myScoreRecord = (ScoreRecord) iManageRecordService.getMyScoreToBook(user_id, book_id).getData();
        String avgScore = bookHouse.getAvg_score();
        if (myScoreRecord != null) {
            bookHouseVO.setMyScore(myScoreRecord.getScore());
        } else {
            bookHouseVO.setMyScore(0);
        }
        bookHouse.setAvg_score(avgScore);
        bookHouseVO.setBookHouse(bookHouse);
        BookRecordUtils.formatBookTime(bookHouse, bookHouseVO);
        bookHouseVO.setBook_type_a_des(bookTypeDes);
        bookHouseVO.setCountry_des(country_des);
        bookDetailsVO.setBookHouseVO(bookHouseVO);
        bookDetailsVO.setCommentRecords(commentRecords);
        bookDetailsVO.setScoreRecords(scoreRecords);
        bookDetailsVO.setRecommendBooks(recommendBooks);
        return ResponseResult.success(bookDetailsVO);
    }

    @Override
    public ResponseResult plusOne(String bookNumOpt, String book_id) {//加一操作
        int i = bookHouseMapper.updatePluNum(bookNumOpt, book_id);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }

    }

    @Override
    public ResponseResult subOne(String bookNumOpt, String book_id) {
        int i = bookHouseMapper.updateSubNuM(bookNumOpt, book_id);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }

    }


    @Override
    public ResponseResult getBookTypeDes(String book_type_code) {
        String bookTypeDes = bookTypeReferMapper.selectBookTypeDes(book_type_code);
        return ResponseResult.success("", bookTypeDes);
    }

    @Override
    public ResponseResult getBookTypeCode(String book_type_des) {
        String bookTypeCode = bookTypeReferMapper.selectBookTypeCode(book_type_des);
        return ResponseResult.success("", bookTypeCode);
    }

    @Override
    public ResponseResult getParentBookType() {
        List<BookTypeRefer> bookTypeReferList = bookTypeReferMapper.selectParentBookType();
        return ResponseResult.success(bookTypeReferList);
    }

    @Override
    public ResponseResult getChildBookType(String parent_code) {
        List<BookTypeRefer> bookTypeReferList = bookTypeReferMapper.selectChildBookType(parent_code);
        return ResponseResult.success(bookTypeReferList);
    }

    @Override
    public ResponseResult getParentChildList() {
        List<BookTypeRefer> parentBookTypes = (List<BookTypeRefer>) getParentBookType().getData();
        List<BookTypeMapVO> bookTypeMapVOS = new ArrayList<>();
        for (BookTypeRefer parentBookType : parentBookTypes) {
            BookTypeMapVO bookTypeMapVO = new BookTypeMapVO();
            List<BookTypeRefer> childBookTypes = (List<BookTypeRefer>) getChildBookType(parentBookType.getBook_type_code()).getData();
            bookTypeMapVO.setParent(parentBookType);
            bookTypeMapVO.setChild(childBookTypes);
            bookTypeMapVOS.add(bookTypeMapVO);
        }
        return ResponseResult.success(bookTypeMapVOS);
    }

    @Override
    public ResponseResult getNumSoreBooks(String bookSortOpt, BookHouse bookHouse) {
        String book_a_type = bookHouse.getBook_a_type() == null ? null : bookHouse.getBook_a_type();
        String book_b_type = bookHouse.getBook_b_type() == null ? null : bookHouse.getBook_b_type();
        String book_c_type = bookHouse.getBook_c_type() == null ? null : bookHouse.getBook_c_type();
        String book_d_type = bookHouse.getBook_d_type() == null ? null : bookHouse.getBook_d_type();
        String avg_score = bookHouse.getAvg_score() == null ? null : bookHouse.getAvg_score();
        List<BookHouse> books = bookHouseMapper.selectOrderBooks(bookSortOpt,
                book_a_type, book_b_type, book_c_type, book_d_type, avg_score);
        return ResponseResult.success(books);
    }


    @Override
    public ResponseResult getMaxTypeNumBook(String bookNumOpt, String book_d_type) {
        List<BookHouse> books = bookHouseMapper.selectMaxTypeNumBooks(bookNumOpt, book_d_type);
        return ResponseResult.success(books);
    }


    @Override
    public ResponseResult modifyScore(String book_id, String avg_score) {
        int i = bookHouseMapper.updateAvgScore(book_id, avg_score);
        return ResponseResult.success();
    }


}
