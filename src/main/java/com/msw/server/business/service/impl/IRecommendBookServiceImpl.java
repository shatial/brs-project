package com.msw.server.business.service.impl;

import com.msw.server.business.entity.BookHouse;
import com.msw.server.business.entity.BrowseRecord;
import com.msw.server.business.entity.UserConfig;
import com.msw.server.business.entity.UserInfo;
import com.msw.server.business.service.*;
import com.msw.server.common.algorithm.RecommendAlgorithm;
import com.msw.server.common.consts.BusConst;
import com.msw.server.common.param.RecommendParam;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.common.param.SearchParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.SequenceInputStream;
import java.util.*;

@Slf4j
@Service
public class IRecommendBookServiceImpl implements IRecommendBooksService {
    @Autowired
    private IManageRecordService iManageRecordService;
    @Autowired
    private IManagerBookService iManagerBookService;
    @Autowired
    private IManageUserService iManageUserService;
    @Autowired
    private IManageUserConfigService iManageUserConfigService;

    @Override
    public ResponseResult recommendBooksByUserWork(RecommendParam recommendParam) {
        BookHouse bookHouse = new BookHouse();
        //获取用户信息
        UserInfo userInfo = (UserInfo) iManageUserService.getUserInfo(recommendParam.getUser_id()).getData();
        if (userInfo == null || userInfo.getWork_a_type() == null || userInfo.getWork_a_type().equals("")) {
            return ResponseResult.success();
        } else {
            //设置查询图书类型
            bookHouse.setBook_b_type(userInfo.getWork_a_type());
            bookHouse.setBook_c_type(userInfo.getWork_b_type());
            bookHouse.setBook_d_type(userInfo.getWork_c_type());
            bookHouse.setAvg_score(recommendParam.getResultScore());
            List<BookHouse> bookHouses = (List<BookHouse>) iManagerBookService.getNumSoreBooks(recommendParam.getBookSortOpt(), bookHouse).getData();
            ///排除用户已读的图书
            List<BookHouse> recommendBooks = getUserNoReadBooksInRecommendBooks(recommendParam.getUser_id(), bookHouses);
            return ResponseResult.success(recommendBooks);
        }

    }

    @Override
    public ResponseResult recommendBooksByLatest(RecommendParam recommendParam) {
        BookHouse bookHouse = new BookHouse();
        //获取最新上架的图书
        recommendParam.setBookSortOpt(BusConst.BUS_BOOK_PUT_TIME);
        List<BookHouse> bookHouses = (List<BookHouse>) iManagerBookService.getNumSoreBooks(recommendParam.getBookSortOpt(), bookHouse).getData();
        List<BookHouse> recommendBooks = getUserNoReadBooksInRecommendBooks(recommendParam.getUser_id(), bookHouses);
        return ResponseResult.success(recommendBooks);
    }

    @Override
    public ResponseResult recommendBooksByScoreRecord(RecommendParam recommendParam) throws Exception {
        List<BookHouse> scoreBooks = (List<BookHouse>) iManageRecordService.getScoreRecord(recommendParam.getUser_id()).getData();
        BookHouse book = new BookHouse();
        if (scoreBooks == null || scoreBooks.size() <= 0) {
            return ResponseResult.success();
        } else {//推荐基于用户
            List<BookHouse> books = new ArrayList<>();
            List<String> bookIds = RecommendAlgorithm.itemBase(recommendParam.getUser_id(), 2);
            if (bookIds != null && bookIds.size() > 0) {
                for (String bookId : bookIds) {
                    BookHouse bookHouse = (BookHouse) iManagerBookService.getBooksByBookId(bookId).getData();
                    books.add(bookHouse);
                }
            } else {
                //获取最大图书类型d
                String type = (String) iManageRecordService.getMaxAvgScoreType(recommendParam.getUser_id()).getData();
                book.setBook_d_type(type);
                book.setAvg_score(recommendParam.getResultScore());
                List<BookHouse> recommendBooks = (List<BookHouse>) iManagerBookService.getNumSoreBooks(recommendParam.getBookSortOpt(), book).getData();
                if (recommendBooks == null || recommendBooks.size() <= 0) {
                    return ResponseResult.success();
                } else {
                    books = recommendBooks;
                }
            }
            List<BookHouse> recommendBooks = getUserNoReadBooksInRecommendBooks(recommendParam.getUser_id(), books);
            return ResponseResult.success(recommendBooks);
        }

    }


    @Override
    public ResponseResult recommendBooksByBrowseRecord(RecommendParam recommendParam) {
        BookHouse bookHouse = new BookHouse();
        //获取最新浏览记录
        BrowseRecord browseRecord = (BrowseRecord) iManageRecordService.getUserLatestBrowseRecords(recommendParam.getUser_id()).getData();
        if (browseRecord == null) {
            return ResponseResult.success();
        } else {
            //设置类型以及得分
            bookHouse.setBook_d_type(browseRecord.getBook_type());
            bookHouse.setAvg_score(recommendParam.getResultScore());
            List<BookHouse> bookHouses = (List<BookHouse>) iManagerBookService.getNumSoreBooks(recommendParam.getBookSortOpt(), bookHouse).getData();
            List<BookHouse> recommendBooks = getUserNoReadBooksInRecommendBooks(recommendParam.getUser_id(), bookHouses);
            return ResponseResult.success(recommendBooks);
        }

    }

    @Override
    public ResponseResult recommendBooksByCollectRecord(RecommendParam recommendParam) {
        BookHouse bookHouse = new BookHouse();
        //获取最大收藏类型
        String type = (String) iManageRecordService.getMaxCountType(recommendParam.getUser_id()).getData();
        if (type == null) {
            return ResponseResult.success();
        } else {
            bookHouse.setBook_d_type(type);
            bookHouse.setAvg_score(recommendParam.getResultScore());
            List<BookHouse> bookHouses = (List<BookHouse>) iManagerBookService.getNumSoreBooks(recommendParam.getBookSortOpt(), bookHouse).getData();
            List<BookHouse> recommendBooks = getUserNoReadBooksInRecommendBooks(recommendParam.getUser_id(), bookHouses);
            return ResponseResult.success(recommendBooks);
        }

    }


    @Override
    public ResponseResult recommendSimilarBooks(RecommendParam recommendParam) {
        log.info("正在查询相似图书");
        String book_id = recommendParam.getBook_id();
        BookHouse bookHouse = (BookHouse) iManagerBookService.getBooksByBookId(book_id).getData();
        SearchParam searchParam = new SearchParam();
        searchParam.setBook_id(bookHouse.getBook_id());
        searchParam.setBook_name(bookHouse.getBook_name());
        searchParam.setBook_d_type(bookHouse.getBook_d_type());
        searchParam.setBook_author(bookHouse.getBook_author());
        List<BookHouse> bookHouses = new ArrayList<>();
        List<BookHouse> recommendBooks = new ArrayList<>();
        //通过书名以及作者名查询图书
        bookHouses = (List<BookHouse>) iManagerBookService.querySimilarBooks(searchParam).getData();
        recommendBooks = getUserNoReadBooksInRecommendBooks(recommendParam.getUser_id(), bookHouses);
        if (recommendBooks == null || recommendBooks.size() <= 0) {//如果没有通过类型查询
            bookHouse.setAvg_score(recommendParam.getResultScore());
            bookHouses = (List<BookHouse>) iManagerBookService.getNumSoreBooks(recommendParam.getBookSortOpt(), bookHouse).getData();
            recommendBooks = getUserNoReadBooksInRecommendBooks(recommendParam.getUser_id(), bookHouses);

        }
        return ResponseResult.success(recommendBooks);
    }


    @Override
    public ResponseResult useRecommendApi(String user_id) throws Exception {
        log.info("正在使用推荐接口api，获取推荐书籍");
        UserConfig userConfig = (UserConfig) iManageUserConfigService.getUserConfig(user_id).getData();
        RecommendParam recommendParam = new RecommendParam();
        recommendParam.setUser_id(user_id);
        recommendParam.setRecommendMode(userConfig.getRecommend_mode());
        recommendParam.setBookSortOpt(userConfig.getSort_mode());
        recommendParam.setResultScore(userConfig.getResult_score());
        if (recommendParam.getBookNumOpt() == null) {
            recommendParam.setBookNumOpt(recommendParam.getBookSortOpt());
        }
        String recommendMode = recommendParam.getRecommendMode();
        log.info("您配置的推荐模式为：" + recommendMode);
        if (recommendMode.equals(BusConst.BUS_REC_MODE_BROWSE)) {
            List<BookHouse> browseBooks = (List<BookHouse>) recommendBooksByBrowseRecord(recommendParam).getData();
            if (browseBooks != null) {
                log.info("您首页的推荐模式为：按浏览记录推荐");
                return ResponseResult.success(browseBooks);
            } else {
                recommendMode = BusConst.BUS_REC_MODE_COLLECT;
            }

        }
        if (recommendMode.equals(BusConst.BUS_REC_MODE_COLLECT)) {
            List<BookHouse> collectBooks = (List<BookHouse>) recommendBooksByCollectRecord(recommendParam).getData();
            if (collectBooks != null) {
                log.info("您首页的推荐模式为：按收藏记录推荐");
                return ResponseResult.success(collectBooks);
            } else {
                recommendMode = BusConst.BUS_REC_MODE_WORK;
            }
        }
        if (recommendMode.equals(BusConst.BUS_REC_MODE_WORK)) {
            List<BookHouse> workBooks = (List<BookHouse>) recommendBooksByUserWork(recommendParam).getData();
            if (workBooks != null) {
                log.info("您首页的推荐模式为：按职业类型推荐");
                return ResponseResult.success(workBooks);
            } else {
                recommendMode = BusConst.BUS_REC_MODE_ALGORITHM;
            }
        }
        if (recommendMode.equals(BusConst.BUS_REC_MODE_ALGORITHM)) {
            List<BookHouse> algorithmBooks = (List<BookHouse>) recommendBooksByScoreRecord(recommendParam).getData();
            if (algorithmBooks != null) {
                log.info("您首页的推荐模式为：按评分记录推荐");
                return ResponseResult.success(algorithmBooks);
            } else {
                recommendMode = BusConst.BUS_REC_MODE_LATEST;
            }
        }
        if (recommendMode.equals(BusConst.BUS_REC_MODE_LATEST)) {
            List<BookHouse> latestBooks = (List<BookHouse>) recommendBooksByLatest(recommendParam).getData();
            if (latestBooks != null) {
                log.info("您首页的推荐模式为：按热门推荐");
                return ResponseResult.success(latestBooks);
            } else {
                return ResponseResult.error();
            }
        }
        return ResponseResult.success();
    }

    @Override
    public List<BookHouse> getUserNoReadBooksInRecommendBooks(String user_id, List<BookHouse> bookLists) {
        log.info("正在去除您已读过的书籍");
        List<BookHouse> books = new ArrayList<>();
        for (BookHouse bookHouse : bookLists) {
            BrowseRecord browseRecord = (BrowseRecord) iManageRecordService.getBrowseRecord(bookHouse.getBook_id(), user_id).getData();
            if (browseRecord == null) {
                books.add(bookHouse);
            }
        }
        return books;
    }
}
