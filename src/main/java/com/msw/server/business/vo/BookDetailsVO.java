package com.msw.server.business.vo;

import com.msw.server.business.entity.BookHouse;
import com.msw.server.business.entity.CommentRecord;
import com.msw.server.business.entity.ScoreRecord;
import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;
import java.util.List;

@ClassAnnotation("书详情VO")
@Data
@ToString
public class BookDetailsVO {

    @PropertyAnnotation("书籍基本信息")
    private BookHouseVO bookHouseVO;

    @PropertyAnnotation("抽取用户对该书的评论记录")
    private List<CommentRecord> commentRecords;

    @PropertyAnnotation("该书的评分情况")
    private List<ScoreRecord> scoreRecords;

    @PropertyAnnotation("推荐的图书")
    private List<BookHouse> recommendBooks;
}
