package com.msw.server.business.vo;

import com.msw.server.business.entity.BookHouse;
import com.msw.server.common.annotation.PropertyAnnotation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BookHouseVO {

    @PropertyAnnotation("图书信息")
    private BookHouse bookHouse;

    @PropertyAnnotation("国家描述")
    private String country_des;

    @PropertyAnnotation("类型描述")
    private String book_type_a_des;

    @PropertyAnnotation("类型描述")
    private String book_type_b_des;

    @PropertyAnnotation("类型描述")
    private String book_type_c_des;

    @PropertyAnnotation("类型描述")
    private String book_type_d_des;

    @ApiModelProperty("出版时间")
    private String publishTime;

    @ApiModelProperty("上架时间")
    private String putTime;

    @PropertyAnnotation("个人给的评分")
    private int myScore;

}
