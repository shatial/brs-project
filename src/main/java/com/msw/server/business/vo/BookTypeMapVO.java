package com.msw.server.business.vo;

import com.msw.server.business.entity.BookTypeRefer;
import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;
import java.util.List;

@ClassAnnotation("用户书库检索设置显示")
@Data
@ToString
public class BookTypeMapVO {

    @PropertyAnnotation("父级")
    private BookTypeRefer parent;

    @PropertyAnnotation("子级集合")
    private List<BookTypeRefer> child;
}
