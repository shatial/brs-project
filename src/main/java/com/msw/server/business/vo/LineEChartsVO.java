package com.msw.server.business.vo;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import java.util.List;

@ClassAnnotation("折线图VO")
@Data
public class LineEChartsVO {

    @PropertyAnnotation("显示的线名")
    private String name;

    @PropertyAnnotation("显示的类型")
    private String type;

    @PropertyAnnotation("显示的一周数量集合")
    private List<Integer> data;
}
