package com.msw.server.business.vo;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import java.util.Date;

@ClassAnnotation("记录VO")
@Data
public class RecordVO {

    @PropertyAnnotation("自增id")
    private long id;

    @PropertyAnnotation("用户id")
    private String user_id;

    @PropertyAnnotation("书号")
    private String book_id;

    @PropertyAnnotation("书名")
    private String book_name;

    @PropertyAnnotation("作者")
    private String book_author;

    @PropertyAnnotation("类型")
    private String book_type;

    @PropertyAnnotation("类型描述")
    private String book_type_des;

    @PropertyAnnotation("创建时间")
    private Date create_time;

    @PropertyAnnotation("创建时间")
    private String createTime;

    @PropertyAnnotation("浏览次数")
    private Long browse_num;

    @PropertyAnnotation("评论内容")
    private String content;

    @PropertyAnnotation("分数")
    private int score;

    @PropertyAnnotation("状态")
    private int status;

}
