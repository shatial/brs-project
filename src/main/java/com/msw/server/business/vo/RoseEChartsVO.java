package com.msw.server.business.vo;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;

@ClassAnnotation("玫瑰图VO")
@Data
@ToString
public class RoseEChartsVO {

    @PropertyAnnotation("数值")
    private int value;

    @PropertyAnnotation("显示名")
    private String name;
}

