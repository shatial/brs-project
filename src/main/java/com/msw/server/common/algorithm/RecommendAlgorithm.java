package com.msw.server.common.algorithm;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.consts.SysConst;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ClassAnnotation("协同过滤算法")
@Component
public class RecommendAlgorithm {

    @MethodAnnotation("获得数据模型")
    public DataModel getDataModel() {
        String file = SysConst.csvFilePath;   //数据集，其中第一列表示用户id；第二列表示商品id；第三列表示评分，评分是5分制
        DataModel model = null;
        try {
            model = new FileDataModel(new File(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return model;
    }

    @MethodAnnotation("基于用户的过滤")
    public static List<String> userBase(String user_id, int userNum, int bookNum) throws Exception {
        long userId = Long.parseLong(user_id);
        List<String> bookIds = new ArrayList<>();
        RecommendAlgorithm recommendAlgorithm = new RecommendAlgorithm();
        DataModel model = recommendAlgorithm.getDataModel();
        UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
        UserNeighborhood neighborhood = new NearestNUserNeighborhood(userNum, similarity, model);
        Recommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
        List<RecommendedItem> recommendations = recommender.recommend(userId, bookNum);
        for (RecommendedItem recommendation : recommendations) {
            bookIds.add(String.valueOf(recommendation.getItemID()));
            System.out.println("书号：" + recommendation.getItemID() + "==>" + "可能存在的评分：" + recommendation.getValue());
        }
        return bookIds;
    }

    @MethodAnnotation("基于物品的过滤")
    public static List<String> itemBase(String user_id, int bookNum) throws Exception {
        long userId = Long.parseLong(user_id);
        List<String> bookIds = new ArrayList<>();
        RecommendAlgorithm recommendAlgorithm = new RecommendAlgorithm();
        DataModel model = recommendAlgorithm.getDataModel();
        ItemSimilarity item = new EuclideanDistanceSimilarity(model);
        Recommender r = new GenericItemBasedRecommender(model, item);
        List<RecommendedItem> recommendations = r.recommend(userId, bookNum);
        for (RecommendedItem recommendation : recommendations) {
            bookIds.add(String.valueOf(recommendation.getItemID()));
            System.out.println("书号：" + recommendation.getItemID() + "==>" + "可能存在的评分：" + recommendation.getValue());
        }
        return bookIds;
    }


}
