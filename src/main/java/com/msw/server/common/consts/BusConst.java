package com.msw.server.common.consts;

import com.msw.server.common.annotation.ClassAnnotation;

@ClassAnnotation("business层常量")
public final class BusConst {
    //用户配置项
    public static final String BUS_BROWSE_MODE = "browse_mode";
    public static final String BUS_COMMENT_MODE = "comment_mode";
    public static final String BUS_MAIL_RECEIVE_MODE = "mail_receive_mode";
    public static final String BUS_SEARCH_MODE = "search_mode";
    public static final String BUS_PSearch_Mode = "PSearchMode";
    public static final String BUS_RSearch_Mode = "RSearchMode";
    public static final String BUS_CSearch_Mode = "CSearchMode";
    public static final String BUS_LSearch_Mode = "LSearchMode";
    public static final String BUS_DSearch_Mode = "DSearchMode";
    public static final String BUS_MSearch_Mode = "MSearchMode";
    public static final String BUS_SORT_MODE = "sort_mode";
    public static final String BUS_RECOMMEND_MODE="recommend_mode";
    //搜索模式可选字段
    public static final String BUS_SEARCH_MODE_BOOK = "book";
    public static final String BUS_SEARCH_MODE_KEYWORD = "keyword";
    //推荐模式可选字段
    public static final String BUS_REC_MODE_BROWSE = "browse";
    public static final String BUS_REC_MODE_WORK = "work";
    public static final String BUS_REC_MODE_LATEST = "latest";
    public static final String BUS_REC_MODE_ALGORITHM = "algorithm";
    public static final String BUS_REC_MODE_COLLECT = "collected";
    //书籍数量类型bookNumOpt以及排序操作bookSortOpt可选字段
    public static final String BUS_BOOK_RECOMMEND = "recommend";
    public static final String BUS_BOOK_COLLECT = "collect";
    public static final String BUS_BOOK_READ = "read";
    public static final String BUS_BOOK_SCORE = "score";
    public static final String BUS_BOOK_PUT_TIME = "putTime";
    //书的父级类型
    public static final String BUS_BOOK_PROFESSION = "11000000";
    public static final String BUS_BOOK_REFERENCE = "12000000";
    public static final String BUS_BOOK_CHILD = "13000000";
    public static final String BUS_BOOK_LITERATURE = "14000000";
    public static final String BUS_BOOK_DOCUMENT = "15000000";
    public static final String BUS_BOOK_COMPLEX = "16000000";


}
