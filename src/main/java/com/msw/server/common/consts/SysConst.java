package com.msw.server.common.consts;

import com.msw.server.common.annotation.ClassAnnotation;

@ClassAnnotation("system层常量")
public final class SysConst {

    public static final String SYS_LOGIN_LOCATION = "黑龙江";
    public static final int SYS_SUCCESS_STATUS = 1;
    public static final int SYS_ERROR_STATUS = 0;
    public static final String SYS_BOOK_IMG = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fdpic.tiankong.com%2F2w%2F60%2FQJ6878138267.jpg&refer=http%3A%2F%2Fdpic.tiankong.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1620190451&t=da518cf1c24af66e93e2daf98ac27e23";
    public static final String csvFilePath = "D:\\Idea2020\\IdeaProject\\brs-project\\src\\main\\resources\\csv\\score.csv";

}
