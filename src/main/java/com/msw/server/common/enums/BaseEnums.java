package com.msw.server.common.enums;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;

@ClassAnnotation("枚举根接口")
public interface BaseEnums {

    @MethodAnnotation("获得状态码")
    int getCode();

    @MethodAnnotation("获得消息")
    String getMsg();
}