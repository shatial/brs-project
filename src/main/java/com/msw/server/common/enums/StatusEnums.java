package com.msw.server.common.enums;

import com.msw.server.common.annotation.ClassAnnotation;
import lombok.AllArgsConstructor;
import lombok.Setter;

@ClassAnnotation("异常枚举类")
@AllArgsConstructor
public enum StatusEnums implements BaseEnums {


    OK(200, "操作成功"),
    ERROR(500, "操作失败"),

    TABLE_OK(0, "操作成功"),
    TABLE_ERROR(500, "无数据"),

    SYS_SEND_MAIL_SUCCESS(200, "邮件发送成功"),
    SYS_SEND_MAIL_ERROR(500, "邮件发送失败"),

    SYS_LOGIN_SUCCESS(200, "登录成功"),
    SYS_CODE_ERROR(500, "验证码错误"),
    SYS_PASSWORD_ERROR(500, "密码错误"),
    SYS_USERNAME_ERROR(500, "账号不存在"),
    SYS_USER_CANCELED_ERROR(500, "该用户已被注销请联系管理员恢复！"),


    SYS_USER_CANCEL_SUCCESS(200, "注销成功"),
    SYS_USER_CANCEL_ERROR(500, "注销失败"),
    SYS_USER_REC_CANCEL_ERROR(500, "该用户已被注销无需重复操作"),

    SYS_USER_RESTORE_SUCCESS(200, "恢复成功"),
    SYS_USER_RESTORE_ERROR(500, "恢复失败"),
    SYS_USER_REC_RESTORE_ERROR(500, "该用户已被恢复无需重复操作"),

    SYS_EXIT_LOGIN_ERROR(500, "退出登录失败"),
    SYS_EXIT_LOGIN_SUCCESS(200, "退出登录成功"),


    SYS_REGISTER_SUCCESS(200, "注册成功"),
    SYS_USER_EXIT(500, "账号已存在"),

    SYS_UPDATE_PASSWORD_SUCCESS(200, "修改成功,下次登录生效"),
    SYS_OLD_NEW_SAME(500, "原密码和新密码相同，修改失败"),
    SYS_NEW_AGAIN_SAME(500, "两次密码不一致，修改失败"),
    SYS_OLD_PASSWORD_ERROR(500, "原密码错误，修改失败"),
    SYS_UPDATE_ERROR(500, "修改失败"),

    SYS_LOGIN_LOG_SUCCESS(200, "登录日志创建成功"),
    SYS_LOGIN_LOG_ERROR(500, "登录日志创建失败"),
    SYS_EXIT_LOG_SUCCESS(200, "退出日志创建成功"),
    SYS_EXIT_LOG_ERROR(500, "退出日志创建失败"),

    SYS_THEME_MAKE_SUCCESS(200, "主题制作成功"),
    SYS_THEME_MAKE_ERROR(500, "主题制作失败"),


    SYS_CLEAR_LOG_SUCCESS(200, "日志清理成功"),
    SYS_CLEAR_LOG_ERROR(500, "日志清理失败"),

    BUS_PERFECT_INFO_SUCCESS(200, "完善个人资料成功"),
    BUS_PERFECT_INFO_ERROR(500, "完善个人资料失败"),

    BUS_BOOK_Exit(500, "已存在该文件，本次只删除"),

    BUS_BOOK_COMMENT_SUCCESS(200, "评论成功"),
    BUS_UPDATE_CONTENT_SUCCESS(200, "更新评论成功"),
    BUS_UPDATE_CONTENT_ERROR(500, "更新评论失败"),

    BUS_BOOK_SCORE_SUCCESS(200, "评分成功"),
    BUS_UPDATE_SCORE_ERROR(500, "更新评分失败"),
    BUS_UPDATE_SCORE_SUCCESS(200, "更新评分成功"),

    BUS_BOOK_UPLOAD_SUCCESS(200, "图书上传成功"),
    BUS_BOOK_UPLOAD_ERROR(500, "图书上传失败"),
    BUS_BOOK_REPEAT(500, "书库已有该图书"),

    BUS_BOOK_COLLECT_SUCCESS(200, "图书收藏成功"),
    BUS_BOOK_EXIST_COLLECT(500, "该图书已经在您的收藏夹中"),

    BUS_BOOK_RECOMMEND_SUCCESS(200, "推荐成功"),
    BUS_BOOK_RECOMMEND_ERROR(500, "推荐失败"),

    BUS_CONFIG_SUCCESS(200, "设置成功"),
    BUS_CONFIG_ERROR(500, "设置失败"),

    BUS_EXECUTE_SUCCESS(200, "生成推荐报告成功"),
    BUS_EXECUTE_ERROR(500, "生成推荐报告失败");


    @Setter
    private int code;

    @Setter
    private String msg;

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}