package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;

@ClassAnnotation("批量删除参数")
@Data
public class DelIdParam {

    @PropertyAnnotation("要删除的主键的集合")
    private long[] ids;

    @PropertyAnnotation("操作数")
    private int opt;


}
