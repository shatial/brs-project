package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@ClassAnnotation("邮箱信息参数")
@Data
@ToString
public class EmailParam implements Serializable {

    @PropertyAnnotation("收件人")
    private String address;

    @PropertyAnnotation("主题")
    private String subject;

    @PropertyAnnotation("信息")
    private String books;

    @PropertyAnnotation("内容")
    private String content;

    @PropertyAnnotation("附件路径")
    private String filepath;

}
