package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;

@ClassAnnotation("代替Map参数")
@Data
@ToString
public class KeyValue {

    @PropertyAnnotation("键")
    private String key;

    @PropertyAnnotation("值")
    private int count;

    @PropertyAnnotation("值")
    private float avgScore;

}
