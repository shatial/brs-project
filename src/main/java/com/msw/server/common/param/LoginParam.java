package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@ClassAnnotation("登录参数")
@Data
@ToString
@Accessors(chain = true)
public class LoginParam {

    @PropertyAnnotation("用户id")
    private String userId;

    @PropertyAnnotation("用户密码")
    private String password;

    @PropertyAnnotation("验证码")
    private String VCode;
}
