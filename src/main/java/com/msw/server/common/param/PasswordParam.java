package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;

@ClassAnnotation("修改密码参数")
@Data
@ToString
public class PasswordParam {

    @PropertyAnnotation("用户账号")
    private String user_id;

    @PropertyAnnotation("旧密码")
    private String old_password;

    @PropertyAnnotation("新密码")
    private String new_password;

    @PropertyAnnotation("重输")
    private String again_password;
}
