package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;


@ClassAnnotation("推荐参数")
@Data
@ToString
public class RecommendParam {

    @PropertyAnnotation("账号操作数")
    private String user_id;

    @PropertyAnnotation("书号操作数")
    private String book_id;

    @PropertyAnnotation("数量操作数")
    private String bookNumOpt;

    @PropertyAnnotation("排序操作数")
    private String bookSortOpt;

    @PropertyAnnotation("推荐模式操作数")
    private String recommendMode;

    @PropertyAnnotation("分数操作数")
    private String resultScore;

}
