package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.experimental.Accessors;

@ClassAnnotation("记录参数")
@Data
@Accessors(chain = true)
public class RecordParam {

    @PropertyAnnotation("用户账号")
    private String user_id;

    @PropertyAnnotation("评论内容")
    private String content;

    @PropertyAnnotation("状态")
    private int status;

    @PropertyAnnotation("评分")
    private int score;

    private long browse_num ;
}
