package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import com.msw.server.common.enums.BaseEnums;
import com.msw.server.common.enums.StatusEnums;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ClassAnnotation("返回值类")
@Data
@Accessors(chain = true)
public class ResponseResult implements Serializable {

    @PropertyAnnotation(value = "状态码")
    private Integer code;

    @PropertyAnnotation(value = "提示信息")
    private String msg;

    @PropertyAnnotation(value = "返回数据条数")
    private long count;

    @PropertyAnnotation(value = "返回数据")
    private Object data;

    @MethodAnnotation("构造方法")
    private static ResponseResult resultData(Integer code, String msg, Object data) {
        ResponseResult res = new ResponseResult();
        res.setCode(code);
        res.setMsg(msg);
        res.setData(data);
        return res;
    }

    @MethodAnnotation("构造方法")
    private static ResponseResult resultData(Integer code, String msg, long count, Object data) {
        ResponseResult res = new ResponseResult();
        res.setCode(code);
        res.setMsg(msg);
        res.setData(data);
        res.setCount(count);
        return res;
    }

    @MethodAnnotation("无参数")
    public static ResponseResult success() {
        return resultData(StatusEnums.OK.getCode(), StatusEnums.OK.getMsg(), null);
    }

    @MethodAnnotation("自定义信息")
    public static ResponseResult success(String msg) {
        return resultData(StatusEnums.OK.getCode(), msg, null);
    }

    @MethodAnnotation("自定义枚举业务操作")
    public static ResponseResult success(BaseEnums enums) {
        return resultData(enums.getCode(), enums.getMsg(), null);
    }

    @MethodAnnotation("自定义数据")
    public static ResponseResult success(Object data) {
        return resultData(StatusEnums.OK.getCode(), StatusEnums.OK.getMsg(), data);
    }
    @MethodAnnotation("自定义数量")
    public static ResponseResult success(long count) {
        return resultData(StatusEnums.OK.getCode(), StatusEnums.OK.getMsg(),count, null);
    }
    @MethodAnnotation("自定义信息和数据")
    public static ResponseResult success(String msg, Object data) {
        return resultData(StatusEnums.OK.getCode(), msg, data);
    }

    @MethodAnnotation("自定义数据和数量用于表格")
    public static ResponseResult success(long count, Object data) {
        return resultData(StatusEnums.TABLE_OK.getCode(), StatusEnums.TABLE_OK.getMsg(), count, data);
    }

    @MethodAnnotation("无参数")
    public static ResponseResult error() {
        return resultData(StatusEnums.ERROR.getCode(), StatusEnums.ERROR.getMsg(), null);
    }

    @MethodAnnotation("自定义信息")
    public static ResponseResult error(String msg) {
        return resultData(StatusEnums.ERROR.getCode(), msg, null);
    }

    @MethodAnnotation("自定义信息和数据")
    public static ResponseResult error(String msg, Object data) {
        return resultData(StatusEnums.ERROR.getCode(), msg, data);
    }

    @MethodAnnotation("自定义数据")
    public static ResponseResult error(Object data) {
        return resultData(StatusEnums.ERROR.getCode(), StatusEnums.ERROR.getMsg(), data);
    }

    @MethodAnnotation("自定义枚举操作")
    public static ResponseResult error(BaseEnums enums) {
        return resultData(enums.getCode(), enums.getMsg(), null);
    }

    @MethodAnnotation("自定义数据和数量用于表格")
    public static ResponseResult error(long count, Object data) {
        return resultData(StatusEnums.TABLE_ERROR.getCode(), StatusEnums.TABLE_ERROR.getMsg(), count, data);
    }
}