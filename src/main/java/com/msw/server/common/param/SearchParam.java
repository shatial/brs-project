package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;

@ClassAnnotation("搜索参数")
@Data
@ToString
public class SearchParam {

    @PropertyAnnotation("书号")
    private String book_id;

    @PropertyAnnotation("书名")
    private String book_name;

    @PropertyAnnotation("作者")
    private String book_author;

    @PropertyAnnotation("类型")
    private String book_a_type;

    @PropertyAnnotation("类型")
    private String book_d_type;

    @PropertyAnnotation("关键字")
    private String desc;
}
