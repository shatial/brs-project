package com.msw.server.common.param;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;

@ClassAnnotation("用户配置参数")
@Data
@ToString
public class UserConfigParam {

    @PropertyAnnotation("用户账号")
    private String user_id;

    @PropertyAnnotation("设置code")
    private String set_code;

    @PropertyAnnotation("值")
    private String set_value;
}
