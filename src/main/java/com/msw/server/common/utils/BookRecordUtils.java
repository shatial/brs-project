package com.msw.server.common.utils;

import com.msw.server.business.entity.*;
import com.msw.server.business.vo.BookHouseVO;
import com.msw.server.business.vo.RecordVO;
import com.msw.server.business.vo.RoseEChartsVO;
import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.consts.BusConst;
import com.msw.server.common.consts.SysConst;
import com.msw.server.common.param.KeyValue;
import com.msw.server.common.param.RecordParam;
import com.msw.server.common.param.UserConfigParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@ClassAnnotation("图书记录工具类")
public class BookRecordUtils {
    @MethodAnnotation("图书信息向其他记录的转换")
    public static Object getRecord(BookHouse bookHouse, Object object, RecordParam recordParam) {
        if (object instanceof BrowseRecord) {
            BrowseRecord browseRecord = new BrowseRecord();
            browseRecord.setUser_id(recordParam.getUser_id());
            browseRecord.setBook_id(bookHouse.getBook_id());
            browseRecord.setBook_name(bookHouse.getBook_name());
            browseRecord.setBook_author(bookHouse.getBook_author());
            browseRecord.setBook_type(bookHouse.getBook_d_type());
            browseRecord.setCreate_time(new Date());
            browseRecord.setBrowse_num(recordParam.getBrowse_num() + 1);
            return browseRecord;
        } else if (object instanceof CollectRecord) {
            CollectRecord collectRecord = new CollectRecord();
            collectRecord.setUser_id(recordParam.getUser_id());
            collectRecord.setBook_id(bookHouse.getBook_id());
            collectRecord.setBook_name(bookHouse.getBook_name());
            collectRecord.setBook_author(bookHouse.getBook_author());
            collectRecord.setBook_type(bookHouse.getBook_d_type());
            collectRecord.setCreate_time(new Date());
            return collectRecord;
        } else if (object instanceof CommentRecord) {
            CommentRecord commentRecord = new CommentRecord();
            commentRecord.setUser_id(recordParam.getUser_id());
            commentRecord.setBook_id(bookHouse.getBook_id());
            commentRecord.setBook_name(bookHouse.getBook_name());
            commentRecord.setBook_author(bookHouse.getBook_author());
            commentRecord.setBook_type(bookHouse.getBook_d_type());
            commentRecord.setCreate_time(new Date());
            commentRecord.setContent(recordParam.getContent());
            return commentRecord;
        } else if (object instanceof RecommendRecord) {
            RecommendRecord recommendRecord = new RecommendRecord();
            recommendRecord.setUser_id(recordParam.getUser_id());
            recommendRecord.setBook_id(bookHouse.getBook_id());
            recommendRecord.setBook_name(bookHouse.getBook_name());
            recommendRecord.setBook_author(bookHouse.getBook_author());
            recommendRecord.setBook_type(bookHouse.getBook_d_type());
            recommendRecord.setCreate_time(new Date());
            return recommendRecord;
        } else {
            ScoreRecord scoreRecord = new ScoreRecord();
            scoreRecord.setUser_id(recordParam.getUser_id());
            scoreRecord.setBook_id(bookHouse.getBook_id());
            scoreRecord.setBook_name(bookHouse.getBook_name());
            scoreRecord.setBook_author(bookHouse.getBook_author());
            scoreRecord.setBook_type(bookHouse.getBook_d_type());
            scoreRecord.setCreate_time(new Date());
            scoreRecord.setScore(recordParam.getScore());
            return scoreRecord;
        }

    }

    public static BookHouse getBookHouse(UploadRecord uploadRecord) {
        BookHouse bookHouse = new BookHouse();
        bookHouse.setBook_name(uploadRecord.getBook_name());
        bookHouse.setBook_author(uploadRecord.getBook_author());
        bookHouse.setBook_img_url("/images/默认.jpg");
        bookHouse.setBook_a_type(uploadRecord.getBook_a_type());
        bookHouse.setBook_b_type(uploadRecord.getBook_b_type());
        bookHouse.setBook_c_type(uploadRecord.getBook_c_type());
        bookHouse.setBook_d_type(uploadRecord.getBook_d_type());
        bookHouse.setBook_feature(uploadRecord.getBook_feature());
        bookHouse.setPublish_unit(uploadRecord.getPublish_unit());
        bookHouse.setBook_price(uploadRecord.getBook_price());
        bookHouse.setPublish_time(uploadRecord.getPublish_time());
        bookHouse.setBook_pages(uploadRecord.getBook_pages());
        bookHouse.setTrial_age(uploadRecord.getTrial_age());
        bookHouse.setBook_country(uploadRecord.getBook_country());
        return bookHouse;
    }

    @MethodAnnotation("设置用户书库显示的检索模式")
    public static void setConfigBookTypes(BookHouse bookHouse, UserConfig userConfig) {
        if (bookHouse.getBook_a_type().equals(BusConst.BUS_BOOK_PROFESSION)) {
            String pSearchMode = userConfig.getPSearchMode();
            String[] pSearchModes = pSearchMode.split(";");
            if (!pSearchModes[0].equals("1")) {
                bookHouse.setBook_b_type(pSearchModes[0]);
            }
            if (!pSearchModes[1].equals("1")) {
                bookHouse.setBook_c_type(pSearchModes[1]);
            }
            if (!pSearchModes[2].equals("1")) {
                bookHouse.setBook_d_type(pSearchModes[2]);
            }
        } else if (bookHouse.getBook_a_type().equals(BusConst.BUS_BOOK_REFERENCE)) {
            String rSearchMode = userConfig.getRSearchMode();
            String[] rSearchModes = rSearchMode.split(";");
            if (!rSearchModes[0].equals("1")) {
                bookHouse.setBook_b_type(rSearchModes[0]);
            }
            if (!rSearchModes[1].equals("1")) {
                bookHouse.setBook_c_type(rSearchModes[1]);
            }
            if (!rSearchModes[2].equals("1")) {
                bookHouse.setBook_d_type(rSearchModes[2]);
            }

        } else if (bookHouse.getBook_a_type().equals(BusConst.BUS_BOOK_CHILD)) {
            String cSearchMode = userConfig.getCSearchMode();
            String[] cSearchModes = cSearchMode.split(";");
            if (!cSearchModes[0].equals("1")) {
                bookHouse.setBook_b_type(cSearchModes[0]);
            }
            if (!cSearchModes[1].equals("1")) {
                bookHouse.setBook_c_type(cSearchModes[1]);
            }
            if (!cSearchModes[2].equals("1")) {
                bookHouse.setBook_d_type(cSearchModes[2]);
            }
        } else if (bookHouse.getBook_a_type().equals(BusConst.BUS_BOOK_LITERATURE)) {
            String lSearchMode = userConfig.getLSearchMode();
            String[] lSearchModes = lSearchMode.split(";");
            if (!lSearchModes[0].equals("1")) {
                bookHouse.setBook_b_type(lSearchModes[0]);
            }
            if (!lSearchModes[1].equals("1")) {
                bookHouse.setBook_c_type(lSearchModes[1]);
            }
            if (!lSearchModes[2].equals("1")) {
                bookHouse.setBook_d_type(lSearchModes[2]);
            }
        } else if (bookHouse.getBook_a_type().equals(BusConst.BUS_BOOK_DOCUMENT)) {
            String dSearchMode = userConfig.getDSearchMode();
            String[] dSearchModes = dSearchMode.split(";");
            if (!dSearchModes[0].equals("1")) {
                bookHouse.setBook_b_type(dSearchModes[0]);
            }
            if (!dSearchModes[1].equals("1")) {
                bookHouse.setBook_c_type(dSearchModes[1]);
            }
            if (!dSearchModes[2].equals("1")) {
                bookHouse.setBook_d_type(dSearchModes[2]);
            }
        } else {
            String mSearchMode = userConfig.getMSearchMode();
            String[] mSearchModes = mSearchMode.split(";");
            if (!mSearchModes[0].equals("1")) {
                bookHouse.setBook_b_type(mSearchModes[0]);
            }
            if (!mSearchModes[1].equals("1")) {
                bookHouse.setBook_c_type(mSearchModes[1]);
            }
            if (!mSearchModes[2].equals("1")) {
                bookHouse.setBook_d_type(mSearchModes[2]);
            }
        }
    }

    @MethodAnnotation("设置用户设置")
    public static UserConfig setUserConfig(UserConfigParam userConfigParam) {
        UserConfig userConfig = new UserConfig();
        String set_code = userConfigParam.getSet_code();
        String set_value = userConfigParam.getSet_value();
        String user_id = userConfigParam.getUser_id();
        userConfig.setUser_id(user_id);
        if (set_code.equals(BusConst.BUS_BROWSE_MODE)) {
            log.info("正在设置浏览模式");
            userConfig.setBrowse_mode(set_value);
        } else if (set_code.equals(BusConst.BUS_COMMENT_MODE)) {
            log.info("正在设置评论模式");
            userConfig.setComment_mode(set_value);
        } else if (set_code.equals(BusConst.BUS_MAIL_RECEIVE_MODE)) {
            log.info("正在设置邮件模式");
            userConfig.setMail_receive_mode(set_value);
        } else if (set_code.equals(BusConst.BUS_SEARCH_MODE)) {
            log.info("正在设置搜索模式");
            userConfig.setSearch_mode(set_value);
        } else if (set_code.equals(BusConst.BUS_PSearch_Mode)) {
            log.info("正在设置书库检索模式");
            userConfig.setPSearchMode(set_value);
        } else if (set_code.equals(BusConst.BUS_RSearch_Mode)) {
            log.info("正在设置书库检索模式");
            userConfig.setRSearchMode(set_value);
        } else if (set_code.equals(BusConst.BUS_CSearch_Mode)) {
            log.info("正在设置书库检索模式");
            userConfig.setCSearchMode(set_value);
        } else if (set_code.equals(BusConst.BUS_LSearch_Mode)) {
            log.info("正在设置书库检索模式");
            userConfig.setLSearchMode(set_value);
        } else if (set_code.equals(BusConst.BUS_DSearch_Mode)) {
            log.info("正在设置书库检索模式");
            userConfig.setDSearchMode(set_value);
        } else if (set_code.equals(BusConst.BUS_MSearch_Mode)) {
            log.info("正在设置书库检索模式");
            userConfig.setMSearchMode(set_value);
        } else if (set_code.equals(BusConst.BUS_SORT_MODE)) {
            log.info("正在设置排序模式");
            userConfig.setSort_mode(set_value);
        } else if (set_code.equals(BusConst.BUS_RECOMMEND_MODE)) {
            log.info("正在设置推荐模式");
            userConfig.setRecommend_mode(set_value);
        } else {
            log.info("正在设置检索结果评分");
            userConfig.setResult_score(set_value);
        }

        return userConfig;

    }

    @MethodAnnotation("设置图书默认值")
    public static void setDefaultBookValue(BookHouse bookHouse, String maxId) {
        if (StringUtils.isBlank(bookHouse.getBook_id())) {
            int id = Integer.valueOf(maxId) + 1;
            maxId = String.valueOf(id);
            bookHouse.setBook_id(maxId);
        }
        if (bookHouse.getBook_img_url() == null || bookHouse.getBook_img_url().length() == 0) {
            bookHouse.setBook_img_url(SysConst.SYS_BOOK_IMG);
        }
        if (StringUtils.isBlank(bookHouse.getPublish_unit())) {
            bookHouse.setPublish_unit(bookHouse.getBook_country());
        }
        if (bookHouse.getPublish_time() == null) {
            bookHouse.setPublish_time(new Date());
        }
        if (bookHouse.getBook_price() == 0.0) {
            bookHouse.setBook_price(50.0);
        }
        if (bookHouse.getTrial_age() == 0) {
            bookHouse.setTrial_age(18);
        }
        if (bookHouse.getCollect_num() == 0) {
            bookHouse.setCollect_num(0);
        }
        if (bookHouse.getRecommend_num() == 0) {
            bookHouse.setRecommend_num(0);
        }
        if (bookHouse.getRead_num() == 0) {
            bookHouse.setRead_num(0);
        }
        if (bookHouse.getAvg_score() == null) {
            bookHouse.setAvg_score("0");
        }
        if (bookHouse.getPublish_time() == null) {
            bookHouse.setPublish_time(new Date());
        }

    }

    public static UserConfig setDefaultUserConfig(String user_id) {
        UserConfig userConfig = new UserConfig();
        userConfig.setUser_id(user_id);
        userConfig.setBrowse_mode("true");
        userConfig.setComment_mode("true");
        userConfig.setMail_receive_mode("true");
        userConfig.setUpload_record_mode("true");
        userConfig.setSearch_mode("book");
        userConfig.setPSearchMode("1;1;1");
        userConfig.setRSearchMode("1;1;1");
        userConfig.setCSearchMode("1;1;1");
        userConfig.setLSearchMode("1;1;1");
        userConfig.setDSearchMode("1;1;1");
        userConfig.setMSearchMode("1;1;1");
        userConfig.setSort_mode("score");
        userConfig.setRecommend_mode("browse");
        userConfig.setResult_score("0");
        return userConfig;
    }

    @MethodAnnotation("格式化记录时间")
    public static void formatRecordTime(List<RecordVO> recordVOS) {
        for (RecordVO recordVO : recordVOS) {
            Date create_time = recordVO.getCreate_time();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String createTime = formatter.format(create_time);
            recordVO.setCreateTime(createTime);
        }

    }

    @MethodAnnotation("格式化书籍时间")
    public static void formatBookTime(BookHouse book, BookHouseVO bookHouseVO) {
        Date publish_time = book.getPublish_time();
        Date put_time = book.getPut_time();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String publishTime = formatter.format(publish_time);
        String putTime = formatter.format(put_time);
        bookHouseVO.setPublishTime(publishTime);
        bookHouseVO.setPutTime(putTime);

    }

    @MethodAnnotation("排序")
    public static void bookSort(List<BookHouse> bookHouses, String bookNumOpt) {
        if (bookNumOpt.equals("recommend")) {
            Collections.sort(bookHouses, new Comparator<BookHouse>() {//根据Value字段排序
                @Override
                public int compare(BookHouse o1, BookHouse o2) {
                    if (o1.getRecommend_num() > o2.getRecommend_num()) {
                        return -1;
                    }
                    if (o1.getRecommend_num() == o2.getRecommend_num()) {
                        return 0;
                    }
                    return 1;
                }
            });
        } else if (bookNumOpt.equals("collect")) {
            Collections.sort(bookHouses, new Comparator<BookHouse>() {//根据Value字段排序
                @Override
                public int compare(BookHouse o1, BookHouse o2) {
                    if (o1.getCollect_num() > o2.getCollect_num()) {
                        return -1;
                    }
                    if (o1.getCollect_num() == o2.getCollect_num()) {
                        return 0;
                    }
                    return 1;
                }
            });
        } else if (bookNumOpt.equals("read")) {
            Collections.sort(bookHouses, new Comparator<BookHouse>() {//根据Value字段排序
                @Override
                public int compare(BookHouse o1, BookHouse o2) {
                    if (o1.getRead_num() > o2.getRead_num()) {
                        return -1;
                    }
                    if (o1.getRead_num() == o2.getRead_num()) {
                        return 0;
                    }
                    return 1;
                }
            });
        } else if (bookNumOpt.equals("score")) {
            bookHouses.sort(new Comparator<BookHouse>() {
                @Override
                public int compare(BookHouse o1, BookHouse o2) {
                    return (int) (Float.valueOf(o2.getAvg_score()) - Float.valueOf(o1.getAvg_score()));
                }
            });
        }

    }

    @MethodAnnotation("创建csv文件")
    public static void createCSVFile(List<ScoreRecord> scoreRecords) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(SysConst.csvFilePath));
            for (ScoreRecord s : scoreRecords) {
                //把学生对象的数据拼接成指定格式的字符串
                StringBuilder sb = new StringBuilder();
                sb.append(s.getUser_id()).append(",").append(s.getBook_id()).append(",").append(s.getScore());
                //调用字符缓冲输出流对象的方法写数据
                bw.write(sb.toString());
                bw.newLine();
                bw.flush();
            }
            //释放资源
            bw.close();
        } catch (Exception e) {
            System.out.println("创建csv文件失败");
        }

    }

    @MethodAnnotation("对kayValue对象字段排序")
    public static void keyValueSort(List<KeyValue> keyValues, String sortField) {
        if (sortField.equals("count")) {
            Collections.sort(keyValues, new Comparator<KeyValue>() {//根据Value字段排序
                @Override
                public int compare(KeyValue o1, KeyValue o2) {
                    if (o1.getCount() > o2.getCount()) {
                        return -1;
                    }
                    if (o1.getCount() == o2.getCount()) {
                        return 0;
                    }
                    return 1;
                }
            });

        } else {
            Collections.sort(keyValues, new Comparator<KeyValue>() {//根据Value字段排序
                @Override
                public int compare(KeyValue o1, KeyValue o2) {
                    if (o1.getAvgScore() > o2.getAvgScore()) {
                        return -1;
                    }
                    if (o1.getAvgScore() == o2.getAvgScore()) {
                        return 0;
                    }
                    return 1;
                }
            });

        }

    }


}
