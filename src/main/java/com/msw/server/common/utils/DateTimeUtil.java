package com.msw.server.common.utils;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ClassAnnotation("时间的计算")
public class DateTimeUtil {
    @MethodAnnotation("根据秒数换算时间")
    public static String formatSecondDateTime(int second) {
        if (second <= 0) return "";
        int h = second / 3600;
        int m = second % 3600 / 60;
        int s = second % 60; //不足60的就是秒，够60就是分
        return h + "小时" + m + "分钟" + s + "秒";
    }

    @SuppressWarnings("deprecation")
    public static List<Date> dateToWeek(Date mdate) {
        int b = mdate.getDay();
        Date fdate;
        List<Date> list = new ArrayList<Date>();
        Long fTime = mdate.getTime() - b * 24 * 3600000;
        for (int a = 1; a <= 7; a++) {
            fdate = new Date();
            fdate.setTime(fTime + (a * 24 * 3600000));
            list.add(a - 1, fdate);
        }
        return list;
    }

    public static List<String> getWeekTime() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//EEE代表星期几
        Date currentDate = new Date();
        List<Date> days = dateToWeek(currentDate);
        List<String> weekTimes = new ArrayList<>();
        for (Date date : days) {
            weekTimes.add(sdf.format(date));
        }
        return weekTimes;
    }


    public static String getNextDay(String day) {
        DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        String nextDay = "";
        try {
            Date temp = dft.parse(day);
            Calendar cld = Calendar.getInstance();
            cld.setTime(temp);
            cld.add(Calendar.DATE, 1);
            temp = cld.getTime();
            //获得下一天日期字符串
            nextDay = dft.format(temp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return nextDay;
    }


}
