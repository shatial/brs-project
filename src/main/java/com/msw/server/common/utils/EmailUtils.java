package com.msw.server.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.msw.server.business.entity.BookHouse;
import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.param.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Slf4j
//@Component
public class EmailUtils {

    /**
     * 发件人地址
     */
    @Value("${spring.mail.username}")
    public String SEND_ADDRESS;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private TemplateEngine templateEngine;

    /**
     * 发送文本邮件
     *
     * @param address 收件人地址
     * @param subject 标题
     * @param content 内容
     */
    public ResponseResult sendTextMail(String address, String subject, String content) {
        if (StringUtils.isBlank(address)) {
            return ResponseResult.error("收件人地址不能为空");
        }
        if (StringUtils.isBlank(subject)) {
            return ResponseResult.error("主题不能为空");
        }
        if (!isEmailAddress(address)) {
            return ResponseResult.error("邮箱地址格式不正确");
        }
        try {
            // 编辑发送邮件的一些信息，有-->发件人地址，收件人地址，邮件标题，邮件正文
            SimpleMailMessage message = new SimpleMailMessage();
            // 发件人地址
            message.setFrom(SEND_ADDRESS);
            // 收件人地址，可多个，使用逗号隔开
            message.setTo(address.split(","));
            // 邮件标题
            message.setSubject(subject);
            // 邮件正文
            message.setText(content);
            // private JavaMailSender mailSender;-->使用该类的send()方法发送邮件
            mailSender.send(message);
            return ResponseResult.success();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseResult.error();
    }

    /**
     * 发送Html邮件
     */
    public ResponseResult sendHtmlMail(String address, String subject, String content) {
        if (StringUtils.isBlank(address)) {
            return ResponseResult.error("收件人地址不能为空");
        }
        if (StringUtils.isBlank(subject)) {
            return ResponseResult.error("主题不能为空");
        }
        if (!isEmailAddress(address)) {
            return ResponseResult.error("邮箱地址格式不正确");
        }
        try {
            MimeMessage message = mailSender.createMimeMessage();
            // 这里与发送文本邮件有所不同
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(SEND_ADDRESS);
            helper.setTo(address.split(","));
            helper.setSubject(subject);
            // 发送HTML邮件，也就是将邮件正文使用HTML的格式书写
            helper.setText(content, true);
            mailSender.send(message);
            return ResponseResult.success();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseResult.error();
    }

    /**
     * 发送附件邮件
     *
     * @param address  收件人地址
     * @param subject  标题
     * @param content  内容
     * @param filePath 文件路径
     */
    public ResponseResult sendAttachmentMail(String address, String subject, String content, String filePath) {
        if (StringUtils.isBlank(address)) {
            return ResponseResult.error("收件人地址不能为空");
        }
        if (StringUtils.isBlank(subject)) {
            return ResponseResult.error("主题不能为空");
        }
        if (!isEmailAddress(address)) {
            return ResponseResult.error("邮箱地址格式不正确");
        }
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(SEND_ADDRESS);
            helper.setTo(address.split(","));
            helper.setSubject(subject);
            helper.setText(content, true);
            // 读取文件
            FileSystemResource file = new FileSystemResource(new File(filePath));
            // 取得文件名
            String fileName = file.getFilename();
            helper.addAttachment(fileName, file);
            mailSender.send(message);
            return ResponseResult.success();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseResult.error();
    }


    /**
     * 判断该邮件地址是否合法
     *
     * @param address 邮件地址，可以多个，逗号隔开
     * @return
     */
    public static boolean isEmailAddress(String address) {
        // 是否合法
        boolean flag = false;
        if (StringUtils.isBlank(address)) {
            return false;
        }
        try {
            String[] addressArr = address.split(",");
            String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
            Pattern regex = Pattern.compile(check);
            Matcher matcher = null;
            for (String str : addressArr) {
                matcher = regex.matcher(str);
                flag = matcher.matches();
                if (!flag) {
                    return false;
                }
            }
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }

    @MethodAnnotation("给用户发送信息的格式")
    public static String getEmailContent(List<BookHouse> bookHouses) {
        String content = "";
        for (BookHouse book : bookHouses) {
            content = content + "书名:" + book.getBook_name() + " " + "作者名:" + book.getBook_author() + " ";
        }
        return content;

    }

}
