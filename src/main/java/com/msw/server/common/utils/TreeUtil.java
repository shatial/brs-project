package com.msw.server.common.utils;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.system.entity.Menu;

import java.util.ArrayList;
import java.util.List;

@ClassAnnotation("成树工具类")
public class TreeUtil {
    public static List<Menu> toTree(List<Menu> treeList, int pid) {
        List<Menu> retList = new ArrayList<Menu>();
        for (Menu parent : treeList) {
            if (pid == parent.getPid()) {
                retList.add(findChildren(parent, treeList));
            }
        }
        return retList;
    }

    private static Menu findChildren(Menu parent, List<Menu> treeList) {
        for (Menu child : treeList) {
            if (parent.getId() == child.getPid()) {
                if (parent.getChild() == null) {
                    parent.setChild(new ArrayList<>());
                }
                parent.getChild().add(findChildren(child, treeList));
            }
        }
        return parent;
    }
}
