package com.msw.server.system.controller;

import com.msw.server.common.enums.StatusEnums;
import com.msw.server.common.param.PasswordParam;
import com.msw.server.system.entity.Permission;
import com.msw.server.system.entity.Role;
import com.msw.server.system.kaptcha.KaptchaUtil;
import com.msw.server.common.param.LoginParam;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.system.entity.User;
import com.msw.server.system.service.IAccessControlService;
import com.msw.server.system.service.ILogGenerateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;

@Api(value = "用户管理模块接口")
@Slf4j
@RequestMapping("/sys")
@Controller
public class AccessControlController {

    @Autowired
    private IAccessControlService iAccessControlService;
    @Autowired
    private ILogGenerateService iLogGenerateService;

    @ApiOperation(value = "进入登录页面")
    @RequestMapping("/brs-login")
    public String toLogin() {
        return "system/otherLogin";
    }


    @ApiOperation(value = "注册")
    @ResponseBody
    @PostMapping("/register")
    public ResponseResult register(User user) {
        ResponseResult responseResult = iAccessControlService.addAUser(user);
        return responseResult;
    }

    @ApiOperation(value = "修改密码")
    @ResponseBody
    @PutMapping("/modifyPassword")
    public ResponseResult modifyPassword(PasswordParam password, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        password.setUser_id(user_id);
        ResponseResult responseResult = iAccessControlService.modifyPassword(password);
        return responseResult;
    }

    @ApiOperation(value = "登录验证")
    @ResponseBody
    @GetMapping("/login")
    public ResponseResult Login(LoginParam loginParam, HttpServletRequest request) {
        if (KaptchaUtil.validate(loginParam.getVCode(), request) == false) {
            return ResponseResult.error(StatusEnums.SYS_CODE_ERROR);
        } else {
            //获取当前用户
            Subject subject = SecurityUtils.getSubject();
            //封装用户登录数据
            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(loginParam.getUserId(), loginParam.getPassword());
            try {
                subject.login(usernamePasswordToken);//执行登录方法
                User user = (User) iAccessControlService.getUserByUsername(loginParam.getUserId()).getData();
                if (user.getStatus() == 0) {
                    return ResponseResult.error(StatusEnums.SYS_USER_CANCELED_ERROR);
                } else {
                    request.getSession().setAttribute("user_id", loginParam.getUserId());
                    //创建登录日志
                    iLogGenerateService.createMonitorLog(loginParam.getUserId(), true);
                    return ResponseResult.success(StatusEnums.SYS_LOGIN_SUCCESS);
                }
            } catch (UnknownAccountException e) {//用户名不存在
                return ResponseResult.error(StatusEnums.SYS_USERNAME_ERROR);
            } catch (IncorrectCredentialsException e) {//密码错误
                return ResponseResult.error(StatusEnums.SYS_PASSWORD_ERROR);
            }
        }

    }

    @ApiOperation(value = "退出登录")
    @GetMapping("/exitLogin")
    @ResponseBody
    public ResponseResult exitLogin(HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = iAccessControlService.exitLogin(user_id);
        request.getSession().setAttribute("user_id", null);
        return responseResult;
    }

    @ApiOperation(value = "查询所有用户")
    @GetMapping("/queryAllUser")
    @ResponseBody
    public ResponseResult queryAllUser(int page, int limit, User user) {
        ResponseResult responseResult = iAccessControlService.getAllUsers(page, limit, user);
        return responseResult;
    }

    @ApiOperation(value = "查询所有角色")
    @GetMapping("/queryAllRoles")
    @ResponseBody
    public ResponseResult queryAllRoles(int page, int limit, int status) {
        ResponseResult responseResult = iAccessControlService.getAllRoles(page, limit, status);
        return responseResult;
    }


    @ApiOperation(value = "查询所有权限")
    @GetMapping("/queryAllPermissions")
    @ResponseBody
    public ResponseResult queryAllPermissions(int page, int limit, int status) {
        ResponseResult responseResult = iAccessControlService.getAllPermissions(page, limit, status);
        return responseResult;
    }

    @ApiOperation(value = "注销或恢复用户")
    @PutMapping("/cancelOrRestoreUser")
    @ResponseBody
    public ResponseResult cancelOrRestoreUser(String user_id, int status, int opt) {
        ResponseResult responseResult = iAccessControlService.cancelRestoreUser(user_id, status, opt);
        return responseResult;
    }

    @ApiOperation(value = "编辑角色")
    @PutMapping("/editRole")
    @ResponseBody
    public ResponseResult editRole(Role role) {
        ResponseResult responseResult = iAccessControlService.modifyRole(role);
        return responseResult;
    }

    @ApiOperation(value = "编辑权限")
    @PutMapping("/editPermission")
    @ResponseBody
    public ResponseResult editPermission(Permission permission) {
        ResponseResult responseResult = iAccessControlService.modifyPermission(permission);
        return responseResult;
    }

    @ApiOperation(value = "查询用户可分配的角色")
    @GetMapping("/queryUserAssignableRole")
    @ResponseBody
    public ResponseResult queryUserAssignableRole(String user_id, HttpServletRequest request) {
        if (user_id != null) {
            request.getSession().setAttribute("UserId", user_id);
        } else {
            user_id = (String) request.getSession().getAttribute("UserId");
        }
        ResponseResult responseResult = iAccessControlService.getUserAssignableRole(user_id);
        return responseResult;
    }

    @ApiOperation(value = "增加用户的角色")
    @PostMapping("/addUserRole")
    @ResponseBody
    public ResponseResult addUserRole(HttpServletRequest request, String roleIds) {
        String user_id = (String) request.getSession().getAttribute("UserId");
        ResponseResult responseResult = iAccessControlService.addUserRole(user_id, roleIds);
        return responseResult;
    }


    @ApiOperation(value = "查询角色可分配的权限")
    @GetMapping("/queryRoleAssignablePermission")
    @ResponseBody
    public ResponseResult queryRoleAssignablePermission(String role_id, HttpServletRequest request) {
        if (role_id != null) {
            request.getSession().setAttribute("RoleId", role_id);
        } else {
            role_id = (String) request.getSession().getAttribute("RoleId");
        }
        ResponseResult responseResult = iAccessControlService.getRoleAssignablePermission(role_id);
        return responseResult;
    }

    @ApiOperation(value = "增加角色的权限")
    @PostMapping("/addRolePermission")
    @ResponseBody
    public ResponseResult addRolePermission(HttpServletRequest request, String pIds) {
        String role_id = (String) request.getSession().getAttribute("RoleId");
        ResponseResult responseResult = iAccessControlService.addRolePermission(role_id, pIds);
        return responseResult;
    }


}
