package com.msw.server.system.controller;


import com.msw.server.common.param.ResponseResult;
import com.msw.server.system.entity.Menu;
import com.msw.server.system.entity.Theme;
import com.msw.server.system.vo.ClearCacheVO;
import com.msw.server.system.service.InitMenuService;
import com.msw.server.system.vo.MenuVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.mahout.cf.taste.common.TasteException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

@Api("菜单管理模块接口")
@Slf4j
@RestController
@RequestMapping("/sys")
public class InitMenuController {

    @Resource
    private InitMenuService initMenuService;


    @ApiOperation(value = "初始化菜单")
    @GetMapping("/initMenu")
    public MenuVO initMenu(HttpServletRequest request) {
        log.info("正在初始化您的菜单");
        String user_id = (String) request.getSession().getAttribute("user_id");
        return initMenuService.initMenu(user_id);
    }

    @ApiOperation(value = "得到所有菜单")
    @GetMapping("/getMenus")
    public ResponseResult getMenus(int page, int limit) {
        return initMenuService.queryAllMenu(page, limit);
    }


    @ApiOperation(value = "新增菜单")
    @PostMapping("/addMenu")
    public ResponseResult addMenu(Menu menu) {
        ResponseResult responseResult = initMenuService.addMenu(menu);
        return responseResult;
    }


    @ApiOperation(value = "初始化主题")
    @GetMapping("/initTheme")
    public Theme initTheme(HttpServletRequest request) {
        log.info("正在初始化您的主题");
        String user_id = (String) request.getSession().getAttribute("user_id");
        Theme theme = initMenuService.initTheme(user_id);
        return theme;
    }

    @ApiOperation(value = "制作主题")
    @PutMapping("/makeTheme")
    public ResponseResult makeTheme(Theme theme, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = initMenuService.makeTheme(theme, user_id);
        return responseResult;
    }

    @ApiOperation(value = "清理缓存")
    @GetMapping("/clear")
    public ClearCacheVO clearBowerCache() {
        ClearCacheVO clearCacheVO = new ClearCacheVO();
        clearCacheVO.setCode(1).setMsg("服务端清理缓存成功");
        return clearCacheVO;
    }

    @ApiOperation(value = "初始化首页")
    @GetMapping("/initHomeInfo")
    public ResponseResult initHomeInfo(HttpServletRequest request) throws Exception {
        log.info("正在初始化您的首页");
        String user_id = (String) request.getSession().getAttribute("user_id");
        ResponseResult responseResult = initMenuService.initHomeInfo(user_id);
        return responseResult;
    }

    @ApiOperation(value = "编辑菜单")
    @PutMapping("/editMenu")
    public ResponseResult editMenu(Menu menu) {
        ResponseResult responseResult = initMenuService.modifyMenu(menu);
        return responseResult;
    }


}
