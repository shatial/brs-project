package com.msw.server.system.controller;

import com.msw.server.business.entity.UserConfig;
import com.msw.server.business.service.IManageUserConfigService;

import com.msw.server.common.param.DelIdParam;
import com.msw.server.common.param.ResponseResult;

import com.msw.server.system.entity.MonitorLog;
import com.msw.server.system.service.ILogGenerateService;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Api(value = "日志管理模块接口")
@Controller
@RequestMapping("/sys")
public class OperationLogController {
    @Autowired
    private ILogGenerateService iLogGenerateService;
    @Autowired
    private IManageUserConfigService iManageUserConfigService;

    @ApiOperation(value = "查询邮件日志")
    @GetMapping("/getMails")
    @ResponseBody
    public ResponseResult getMail(int page, int limit, HttpServletRequest request) {
        String receiver_id = (String) request.getSession().getAttribute("user_id");
        //获取用户接收邮件的配置
        UserConfig userConfig = (UserConfig) iManageUserConfigService.getUserConfig(receiver_id).getData();
        if (userConfig.getMail_receive_mode().equals("false")) {//false:不接受邮件
            return ResponseResult.success();
        } else {
            ResponseResult responseResult = iLogGenerateService.getMailLogsBySenderOrReceiver(page, limit, null, receiver_id);
            return responseResult;
        }
    }

    @ApiOperation(value = "删除邮件日志")
    @DeleteMapping("/delMails")
    @ResponseBody
    public ResponseResult delMails(DelIdParam delIdParam) {
        ResponseResult responseResult = iLogGenerateService.delMailLog(delIdParam.getIds());
        return responseResult;
    }

    @ApiOperation(value = "邮件详情")
    @GetMapping("/getMailDetail")
    @ResponseBody
    public ResponseResult getMailDetail(long id) {
        ResponseResult responseResult = iLogGenerateService.getMailDetail(id);
        return responseResult;
    }

    @ApiOperation(value = "查询监控日志")
    @GetMapping("/queryMonitorLog")
    @ResponseBody
    public ResponseResult queryMonitorLog(int page, int limit, MonitorLog monitorLog) {
        ResponseResult responseResult = iLogGenerateService.getMonitorLog(page, limit, monitorLog);
        return responseResult;
    }

    @ApiOperation(value = "查询监控日志")
    @DeleteMapping("/clearMonitorLog")
    @ResponseBody
    public ResponseResult clearMonitorLog(DelIdParam delIdParam) {
        ResponseResult responseResult = iLogGenerateService.clearMonitorLog(delIdParam.getIds());
        return responseResult;
    }
}
