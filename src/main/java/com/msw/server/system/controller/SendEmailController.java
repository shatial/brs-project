package com.msw.server.system.controller;


import com.msw.server.system.service.SendEmailService;
import com.msw.server.common.param.EmailParam;
import com.msw.server.common.param.ResponseResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Api(value = "邮件发送/分享模块接口")
@Controller
@RequestMapping("/api")
public class SendEmailController {

    @Autowired
    private SendEmailService sendEmailService;

    @ApiOperation(value = "发送文本")
    @GetMapping("/sendTextMail")
    @ResponseBody
    public ResponseResult sendTextMail(EmailParam emailParam, HttpServletRequest request) {
        String user_id = (String) request.getSession().getAttribute("user_id");
        return sendEmailService.sendTextMail(emailParam,user_id);
    }

//    @ApiOperation(value = "发送图片")
//    @GetMapping("/sendFileMail")
//    @ResponseBody
//    public ResponseResult sendFileMail(EmailParam emailParam) {
//        return sendEmailService.sendFileMail(emailParam);
//    }


}
