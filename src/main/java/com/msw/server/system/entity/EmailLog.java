package com.msw.server.system.entity;




import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@ClassAnnotation("多条")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "SysEmailLog对象", description = "邮件发送记录表")
public class EmailLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Integer id;

    @ApiModelProperty(value = "邮箱地址")
    private String address;

    @ApiModelProperty(value = "主题")
    private String subject;

    @ApiModelProperty(value = "类型(0--文本1--html2--图片3--附件4--模板)")
    private Integer type;

    @ApiModelProperty(value = "发送状态(0--成功1--失败)")
    private Integer status;

    @ApiModelProperty(value = "创建者")
    private Integer createUser;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新人")
    private Integer updateUser;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "删除状态（0--未删除1--已删除）")
    private Integer delFlag;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "发送人姓名")
    private String createUserName;

    @ApiModelProperty(value = "模板文件")
    public String template;



}
