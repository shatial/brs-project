package com.msw.server.system.entity;

import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
@ClassAnnotation("多条")
@ApiModel(value = "MailLog对象", description = "mail_log表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MailLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private long id;

    @ApiModelProperty(value = "发送者账号")
    private String sender_id;

    @ApiModelProperty(value = "发送者姓名")
    private String sender_name;

    @ApiModelProperty(value = "发送者邮箱")
    private String sender_mail;

    @ApiModelProperty(value = "接收者账号")
    private String receiver_id;

    @ApiModelProperty(value = "接收者姓名")
    private String receiver_name;

    @ApiModelProperty(value = "接收者邮箱")
    private String receiver_mail;

    @ApiModelProperty(value = "主题")
    private String subject;


    @ApiModelProperty(value = "文本")
    private String content;

    @ApiModelProperty(value = "附件路径")
    private String filepath;

    @ApiModelProperty(value = "状态")
    private int status;

    @ApiModelProperty(value = "创建时间")
    private Date create_time;




}
