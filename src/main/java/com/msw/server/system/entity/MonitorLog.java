package com.msw.server.system.entity;

import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
@ClassAnnotation("多条")
@ApiModel(value = "MonitorLog", description = "表monitor_log")
@Data
@ToString
@Accessors(chain = true)
public class MonitorLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    private long  id;

    @ApiModelProperty(value = "用户账号")
    private String user_id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "登录地址")
    private String login_address;

    @ApiModelProperty(value = "登陆时间")
    private Date login_time;

    @ApiModelProperty(value = "下线时间")
    private Date exit_time;

    @ApiModelProperty(value = "状态")
    private int status;

    @ApiModelProperty(value = "在线时长")
    private String keep_time;

}
