package com.msw.server.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "Permission对象", description = "permission")
@Data
@ToString
@Accessors(chain = true)
public class Permission implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "权限id")
    private String pid;

    @ApiModelProperty(value = "权限名")
    private String p_name;

    @ApiModelProperty(value = "标记")
    private String sign;

    @ApiModelProperty(value = "url")
    private String url;

    @ApiModelProperty(value = "状态")
    private int status;

    @ApiModelProperty(value = "描述")
    private String des;

    @ApiModelProperty(value = "创建时间")
    private Date create_time;

    @ApiModelProperty(value = "更新时间")
    private Date update_time;
}
