package com.msw.server.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "Role对象", description = "role")
@Data
@ToString
@Accessors(chain = true)
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色Id")
    private String role_id;

    @ApiModelProperty(value = "角色名")
    private String role_name;

    @ApiModelProperty(value = "角色描述")
    private String des;

    @ApiModelProperty(value = "状态")
    private int status;

    @ApiModelProperty(value = "创建时间")
    private Date create_time;

    @ApiModelProperty(value = "更新时间")
    private Date update_time;

}
