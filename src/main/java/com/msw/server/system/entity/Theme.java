package com.msw.server.system.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class Theme implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    private String user_id;

    private String headerRightBg;//头部右侧背景色

    private String headerRightBgThis = "#e4e4e4";

    private String headerRightColor = "rgba(107, 107, 107, 0.7)";

    private String headerRightChildColor = "rgba(107, 107, 107, 0.7)";

    private String headerRightColorThis = "#565656";

    private String headerRightNavMore = "rgba(160, 160, 160, 0.7)";

    private String headerRightNavMoreBg = "#1E9FFF";

    private String headerRightNavMoreColor = "#ffffff";

    private String headerRightToolColor = "#565656";

    private String headerLogoBg = "#192027";

    private String headerLogoColor = "rgb(191, 187, 187)";

    private String leftMenuNavMore = "rgb(191, 187, 187)";

    private String leftMenuBg;//左侧菜单背景,

    private String leftMenuBgThis;//左侧菜单选中背景,

    private String leftMenuChildBg;//左侧菜单子菜单背景,

    private String leftMenuColor = "rgb(191, 187, 187)";

    private String leftMenuColorThis = "#ffffff";

    private String tabActiveColor = "#1e9fff";

}
