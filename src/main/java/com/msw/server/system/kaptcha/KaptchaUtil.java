package com.msw.server.system.kaptcha;

import com.google.code.kaptcha.Constants;
import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

@ClassAnnotation("验证码工具类")
public class KaptchaUtil {

    @MethodAnnotation("判断验证码正误")
    public static boolean validate(String registerCode, HttpServletRequest request) {
        if (StringUtils.isEmpty(registerCode)) {
            return false;
        }
        // 获取Session中验证码
        Object captcha = request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        if (null == captcha) {
            return false;
        }
        boolean result = registerCode.equalsIgnoreCase(captcha.toString());
        if (result) {
            request.getSession().removeAttribute(Constants.KAPTCHA_SESSION_KEY);
        }
        return result;
    }
}
