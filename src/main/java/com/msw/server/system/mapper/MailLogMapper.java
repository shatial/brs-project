package com.msw.server.system.mapper;


import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.system.entity.MailLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MailLogMapper {
    @MethodAnnotation("增加一条邮件日志")
    public int insertMailLog(MailLog mailLog);

    @MethodAnnotation("根据条件查询邮件日志的集合")
    public List<MailLog> selectMailLogBySenderOrReceiver(String sender_id, String receiver_id);

    @MethodAnnotation("根据条件查询邮件日志的数量")
    public int selectCount(String sender_id, String receiver_id);

    @MethodAnnotation("根据主键查询某条邮件日志")
    public MailLog selectMail(long id);

    @MethodAnnotation("根据主键删除一条邮件日志")
    public int deleteMailLog(long id);
}
