package com.msw.server.system.mapper;


import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.system.entity.Menu;
import com.msw.server.system.entity.Theme;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper {

    public int insertMenu(Menu menu);

    public int selectMaxId();

    @MethodAnnotation("查询所有菜单的集合")
    public List<Menu> selectAllMenu(int isChild);

    public int selectCountMenu(int isChild);

    @MethodAnnotation("查询用户的主题")
    public Theme selectTheme(String user_id);

    @MethodAnnotation("更新主题")
    public int updateTheme(String user_id, String headerRightBg,String leftMenuBg,String leftMenuBgThis,String leftMenuChildBg);

    public int updateMenu(Menu menu);

}
