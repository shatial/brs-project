package com.msw.server.system.mapper;


import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.system.entity.MonitorLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

@Mapper
public interface MonitorLogMapper {
    @MethodAnnotation("增加一条登录日志")
    public int insertMonitorLog(MonitorLog monitorLog);

    @MethodAnnotation("查询某人的一条登录日志")
    public MonitorLog selectAMonitorLog(String user_id, int status);

    @MethodAnnotation("根据状态查询登录日志的集合")
    public List<MonitorLog> selectMonitorLogs(MonitorLog monitorLog);

    public int selectCountLogs(MonitorLog monitorLog);

    @MethodAnnotation("更新某人登录日志的在线时长")
    public int selectKeepTime(String user_id, int status);

    @MethodAnnotation("更新某人的退出时间和在线时间")
    public int updateAMonitorLog(Date exit_time, String keep_time, String user_id);

    @MethodAnnotation("根据状态和时间删除一条登录日志")
    public int deleteMonitorLogs(long id);


}
