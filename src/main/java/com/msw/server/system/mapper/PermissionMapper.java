package com.msw.server.system.mapper;

import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.system.entity.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PermissionMapper {

    public int insertRolePermission(String role_id,String p_id);

    @MethodAnnotation("查询权限标识通过权限id")
    public String selectSignByPId(String pid);

    public List<Permission> selectAllPermission(int status);

    public int selectCountPermission(int status);

    @MethodAnnotation("查询权限id通过角色id")
    public List<String> selectPIdByRoleId(String role_id);

    public int updatePermission(Permission permission);
}
