package com.msw.server.system.mapper;

import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.system.entity.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper {

    @MethodAnnotation("新增用户角色")
    public int insertUserRole(String user_id, String role_id);

    public List<Role> selectAllRoles(int status);

    public int selectCountRoles(int status);

    @MethodAnnotation("查询角色id通过用户id")
    public List<String> selectRoleIdByUserId(String user_id);

    public int updateRole(Role role);


}
