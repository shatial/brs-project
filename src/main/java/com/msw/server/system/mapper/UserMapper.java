package com.msw.server.system.mapper;

import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.system.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    @MethodAnnotation("新增用户")
    public int insertUser(User user);

    public List<User> selectAllUser(User user);

    public int selectCountUser(User user);

    @MethodAnnotation("查询用户通过用户id")
    public User selectUserByUserId(String user_id);

    @MethodAnnotation("更新密码")
    public int updatePassword(String user_id, String password);

    public int updateUserStatus(String user_id, int status);
}
