package com.msw.server.system.service;



import com.msw.server.common.param.PasswordParam;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.system.entity.Permission;
import com.msw.server.system.entity.Role;
import com.msw.server.system.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface IAccessControlService {

    public ResponseResult getUserByUsername(String user_id);

    public ResponseResult getAllUsers(int page, int limit, User user);

    public ResponseResult getAllRoles(int page, int limit, int status);

    public ResponseResult getAllPermissions(int page, int limit, int status);

    public ResponseResult getRoleIdByUserId(String user_id);

    public ResponseResult getPIdByRoleId(String role_id);

    public ResponseResult getSignByPId(String pid);

    public ResponseResult addAUser(User user);

    public ResponseResult modifyPassword(PasswordParam password);

    public ResponseResult exitLogin(String user_id);

    public ResponseResult cancelRestoreUser(String user_id,int status,int opt);

    public ResponseResult modifyRole(Role role);

    public ResponseResult modifyPermission(Permission permission);

    public ResponseResult getUserAssignableRole(String user_id);

    public ResponseResult addUserRole(String user_id,String roleIds);

    public ResponseResult getRoleAssignablePermission(String role_id);

    public ResponseResult addRolePermission(String role_id,String pIds);




}
