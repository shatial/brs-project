package com.msw.server.system.service;


import com.msw.server.common.param.ResponseResult;
import com.msw.server.system.entity.MailLog;
import com.msw.server.system.entity.MonitorLog;

import java.util.Date;


public interface ILogGenerateService {

    public ResponseResult createMonitorLog(String user_id, boolean isLogin);

    public ResponseResult getMonitorLog(int page,int limit, MonitorLog monitorLog);

    public ResponseResult clearMonitorLog(long[] ids);

    public ResponseResult createMailLog(MailLog mailLog);

    public ResponseResult delMailLog(long[] ids);

    public ResponseResult getMailLogsBySenderOrReceiver(int page, int limit, String sender_id, String receiver_id);

    public ResponseResult getMailDetail(long id);
}
