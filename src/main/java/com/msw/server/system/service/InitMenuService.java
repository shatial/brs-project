package com.msw.server.system.service;

import com.msw.server.common.param.ResponseResult;
import com.msw.server.system.entity.Menu;
import com.msw.server.system.entity.Theme;
import com.msw.server.system.vo.MenuVO;
import org.apache.mahout.cf.taste.common.TasteException;

import java.io.IOException;
import java.sql.SQLException;


public interface InitMenuService {

    public MenuVO initMenu(String user_id);

    public ResponseResult addMenu(Menu menu);

    public ResponseResult queryAllMenu(int page,int limit);

    public ResponseResult initHomeInfo(String user_id) throws Exception;

    public Theme initTheme(String user_id);

    public ResponseResult makeTheme(Theme theme,String user_id);

    public ResponseResult modifyMenu(Menu menu);

}
