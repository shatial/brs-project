package com.msw.server.system.service;

import com.msw.server.common.param.EmailParam;
import com.msw.server.common.param.ResponseResult;

public interface SendEmailService {
    public ResponseResult sendTextMail(EmailParam emailParam,String user_id);

    //public ResponseResult sendFileMail(EmailParam emailParam);
}
