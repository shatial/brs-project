package com.msw.server.system.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msw.server.business.service.IManageUserConfigService;

import com.msw.server.common.consts.SysConst;
import com.msw.server.common.enums.StatusEnums;


import com.msw.server.common.param.PasswordParam;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.system.entity.MonitorLog;
import com.msw.server.system.entity.Permission;
import com.msw.server.system.entity.Role;
import com.msw.server.system.entity.User;

import com.msw.server.system.mapper.PermissionMapper;
import com.msw.server.system.mapper.RoleMapper;
import com.msw.server.system.mapper.UserMapper;
import com.msw.server.system.service.IAccessControlService;
import com.msw.server.system.service.ILogGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class IAccessControlServiceImpl implements IAccessControlService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private ILogGenerateService iLogGenerateService;
    @Autowired
    private IManageUserConfigService iManageUserConfigService;

    @Override
    public ResponseResult getUserByUsername(String username) {
        User user = userMapper.selectUserByUserId(username);
        return ResponseResult.success(user);
    }

    @Override
    public ResponseResult getAllUsers(int page, int limit, User user) {
        int count = userMapper.selectCountUser(user);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<User> users = userMapper.selectAllUser(user);
            PageInfo<User> roleUser = new PageInfo<>(users);
            return ResponseResult.success(count, roleUser.getList());
        }

    }

    @Override
    public ResponseResult getAllRoles(int page, int limit, int status) {
        int count = roleMapper.selectCountRoles(status);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<Role> roles = roleMapper.selectAllRoles(status);
            PageInfo<Role> roleRole = new PageInfo<>(roles);
            return ResponseResult.success(count, roleRole.getList());
        }
    }

    @Override
    public ResponseResult getAllPermissions(int page, int limit, int status) {
        int count = permissionMapper.selectCountPermission(status);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<Permission> permissions = permissionMapper.selectAllPermission(status);
            PageInfo<Permission> rolePermission = new PageInfo<>(permissions);
            return ResponseResult.success(count, rolePermission.getList());
        }
    }

    @Override
    public ResponseResult getRoleIdByUserId(String user_id) {
        List<String> rids = roleMapper.selectRoleIdByUserId(user_id);
        return ResponseResult.success("", rids);
    }

    @Override
    public ResponseResult getPIdByRoleId(String role_id) {
        List<String> pIds = permissionMapper.selectPIdByRoleId(role_id);
        return ResponseResult.success("", pIds);
    }

    @Override
    public ResponseResult getSignByPId(String pid) {
        String sign = permissionMapper.selectSignByPId(pid);
        return ResponseResult.success("", sign);
    }

    @Override
    public ResponseResult addAUser(User user) {
        //设置新用户信息
        user.setStatus(SysConst.SYS_SUCCESS_STATUS).setCreate_time(new Date()).setUpdate_time(new Date()).setDes("用户");
        User exitUser = userMapper.selectUserByUserId(user.getUser_id());
        //判断该账户是否已经存在
        if (exitUser != null) {
            return ResponseResult.error(StatusEnums.SYS_USER_EXIT);
        } else {
            int i = userMapper.insertUser(user);
            if (i == 1) {
                //新建用户
                int j = roleMapper.insertUserRole(user.getUser_id(), "user");
                if (j == 1) {
                    iManageUserConfigService.createDefaultUserConfig(user.getUser_id());
                    return ResponseResult.success(StatusEnums.SYS_REGISTER_SUCCESS);
                } else {
                    return ResponseResult.error(StatusEnums.SYS_USER_EXIT);
                }
            } else {
                return ResponseResult.error(StatusEnums.SYS_USER_EXIT);
            }
        }

    }

    @Override
    public ResponseResult modifyPassword(PasswordParam password) {
        //新旧密码不能一样
        if (password.getNew_password().equals(password.getOld_password())) {
            return ResponseResult.error(StatusEnums.SYS_OLD_NEW_SAME);
        } else if (!password.getNew_password().equals(password.getAgain_password())) {
            //两次输入的密码必须一致
            return ResponseResult.error(StatusEnums.SYS_NEW_AGAIN_SAME);
        } else {
            User user = userMapper.selectUserByUserId(password.getUser_id());
            if (!user.getPassword().equals(password.getOld_password())) {
                //旧密码必须与数据库一致
                return ResponseResult.error(StatusEnums.SYS_OLD_PASSWORD_ERROR);
            } else {
                //跟新密码
                int i = userMapper.updatePassword(password.getUser_id(), password.getNew_password());
                if (i == 1) {
                    return ResponseResult.success(StatusEnums.SYS_UPDATE_PASSWORD_SUCCESS);
                } else {
                    return ResponseResult.success(StatusEnums.SYS_UPDATE_ERROR);
                }
            }

        }
    }

    @Override
    public ResponseResult exitLogin(String user_id) {
        //调用创建日志方法
        ResponseResult responseResult = iLogGenerateService.createMonitorLog(user_id, false);
        if (responseResult.getCode() == 200) {
            return ResponseResult.success(StatusEnums.SYS_EXIT_LOGIN_SUCCESS);
        } else {
            return ResponseResult.error(StatusEnums.SYS_EXIT_LOGIN_ERROR);
        }

    }

    @Override
    public ResponseResult cancelRestoreUser(String user_id, int status, int opt) {
        if (opt == status && opt == 1) {
            return ResponseResult.error(StatusEnums.SYS_USER_REC_RESTORE_ERROR);
        } else if (opt == status && opt == 0) {
            return ResponseResult.error(StatusEnums.SYS_USER_REC_CANCEL_ERROR);
        } else {
            int i = userMapper.updateUserStatus(user_id, opt);
            if (i == 1) {
                if (opt == 1) {
                    return ResponseResult.success(StatusEnums.SYS_USER_RESTORE_SUCCESS);
                } else {
                    return ResponseResult.success(StatusEnums.SYS_USER_CANCEL_SUCCESS);
                }
            } else {
                if (opt == 1) {
                    return ResponseResult.success(StatusEnums.SYS_USER_RESTORE_ERROR);
                } else {
                    return ResponseResult.success(StatusEnums.SYS_USER_CANCEL_ERROR);
                }
            }

        }

    }

    @Override
    public ResponseResult modifyRole(Role role) {
        int i = roleMapper.updateRole(role);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }
    }

    @Override
    public ResponseResult modifyPermission(Permission permission) {
        int i = permissionMapper.updatePermission(permission);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }
    }

    @Override
    public ResponseResult getUserAssignableRole(String user_id) {
        List<String> rids = (List<String>) getRoleIdByUserId(user_id).getData();
        List<Role> roles = roleMapper.selectAllRoles(3);
        List<Role> assignableRole = new ArrayList<>();
        boolean f = false;
        for (Role role : roles) {
            for (String rid : rids) {
                if (role.getRole_id().equals(rid)) {
                    f = true;
                }
            }
            if (f == false) {
                assignableRole.add(role);
            }
        }
        return ResponseResult.success(assignableRole.size(), assignableRole);
    }

    @Override
    public ResponseResult addUserRole(String user_id, String roleIds) {
        String[] roleS = roleIds.split(",");
        for (String role_id : roleS) {
            roleMapper.insertUserRole(user_id, role_id);
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult getRoleAssignablePermission(String role_id) {
        List<String> pIds = (List<String>) getPIdByRoleId(role_id).getData();
        List<Permission> permissions = permissionMapper.selectAllPermission(3);
        List<Permission> assignableRole = new ArrayList<>();
        boolean f = false;
        for (Permission permission : permissions) {
            for (String pid : pIds) {
                if (permission.getPid().equals(pid)) {
                    f = true;
                }
            }
            if (f == false) {
                assignableRole.add(permission);
            }
        }
        return ResponseResult.success(assignableRole.size(), assignableRole);
    }

    @Override
    public ResponseResult addRolePermission(String role_id, String pIds) {
        String[] perS = pIds.split(",");
        for (String p_id : perS) {
            permissionMapper.insertRolePermission(role_id, p_id);
        }
        return ResponseResult.success();
    }


}
