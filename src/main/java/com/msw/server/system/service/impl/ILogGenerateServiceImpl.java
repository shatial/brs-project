package com.msw.server.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.consts.SysConst;
import com.msw.server.common.enums.StatusEnums;
import com.msw.server.common.param.ResponseResult;

import com.msw.server.common.utils.DateTimeUtil;
import com.msw.server.system.entity.MailLog;
import com.msw.server.system.entity.MonitorLog;
import com.msw.server.system.entity.User;
import com.msw.server.system.mapper.MailLogMapper;
import com.msw.server.system.mapper.MonitorLogMapper;

import com.msw.server.system.service.IAccessControlService;
import com.msw.server.system.service.ILogGenerateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class ILogGenerateServiceImpl implements ILogGenerateService {
    @Autowired
    private MonitorLogMapper monitorLogMapper;

    @Autowired
    private IAccessControlService iAccessControlService;
    @Autowired
    private MailLogMapper mailLogMapper;

    @MethodAnnotation("创建登录日志")
    @Override
    public ResponseResult createMonitorLog(String user_id, boolean isLogin) {
        if (isLogin == true) {//是登录操作
            MonitorLog monitorLog = new MonitorLog();
            //获得用户信息
            User user = (User) iAccessControlService.getUserByUsername(user_id).getData();
            //创建日志对象
            monitorLog.setId(SysConst.SYS_ERROR_STATUS).setUser_id(user_id).setUsername(user.getUsername()).
                    setLogin_address(SysConst.SYS_LOGIN_LOCATION).setLogin_time(new Date()).setExit_time(new Date()).setStatus(1);
            //查询该用户是否未下线
            MonitorLog existLog = monitorLogMapper.selectAMonitorLog(user_id, SysConst.SYS_SUCCESS_STATUS);
            if (existLog == null) {
                //创建登录日志，有就不创建
                int i = monitorLogMapper.insertMonitorLog(monitorLog);
                if (i == 1) {
                    return ResponseResult.success(StatusEnums.SYS_LOGIN_LOG_SUCCESS);
                } else {
                    return ResponseResult.error(StatusEnums.SYS_LOGIN_LOG_ERROR);
                }
            } else {
                return ResponseResult.success(StatusEnums.SYS_LOGIN_LOG_SUCCESS);
            }
        } else {//退出操作
            //获取用户在线时长
            int second = monitorLogMapper.selectKeepTime(user_id, SysConst.SYS_SUCCESS_STATUS);
            //将秒数格式化m:n:s
            String keep_time = DateTimeUtil.formatSecondDateTime(second);
            //改变用户日志状态
            int i = monitorLogMapper.updateAMonitorLog(new Date(), keep_time, user_id);
            if (i == 1) {
                return ResponseResult.success(StatusEnums.SYS_EXIT_LOG_SUCCESS);
            } else {
                return ResponseResult.error(StatusEnums.SYS_EXIT_LOG_ERROR);
            }
        }

    }

    @Override
    public ResponseResult getMonitorLog(int page, int limit, MonitorLog monitorLog) {
        int count = monitorLogMapper.selectCountLogs(monitorLog);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<MonitorLog> monitorLogs = monitorLogMapper.selectMonitorLogs(monitorLog);
            PageInfo<MonitorLog> roleMonitor = new PageInfo<>(monitorLogs);
            return ResponseResult.success(count, roleMonitor.getList());
        }
    }

    @Override
    public ResponseResult clearMonitorLog(long[] ids) {
        //根据传来的主键id批量删除
        for (int j = 0; j < ids.length; j++) {
            int i = monitorLogMapper.deleteMonitorLogs(ids[j]);
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult createMailLog(MailLog mailLog) {
        int i = mailLogMapper.insertMailLog(mailLog);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }

    }

    @Override
    public ResponseResult delMailLog(long[] ids) {
        //根据传来的主键id批量删除
        for (int j = 0; j < ids.length; j++) {
            int i = mailLogMapper.deleteMailLog(ids[j]);
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult getMailLogsBySenderOrReceiver(int page, int limit, String sender_id, String receiver_id) {
        if (receiver_id == null && sender_id != null) {
            int count = mailLogMapper.selectCount(sender_id, null);//查询记录个数
            if (count == 0) {
                return ResponseResult.error(count, null);
            } else {//分页
                PageHelper.startPage(page, limit);
                List<MailLog> mailLogs = mailLogMapper.selectMailLogBySenderOrReceiver(sender_id, null);
                PageInfo<MailLog> roleMailLogs = new PageInfo<>(mailLogs);
                return ResponseResult.success(count, roleMailLogs.getList());
            }
        } else if (receiver_id != null && sender_id == null) {//查询记录个数
            int count = mailLogMapper.selectCount(null, receiver_id);
            if (count == 0) {
                return ResponseResult.error(count, null);
            } else {//分页
                PageHelper.startPage(page, limit);
                List<MailLog> mailLogs = mailLogMapper.selectMailLogBySenderOrReceiver(null, receiver_id);
                PageInfo<MailLog> roleMailLogs = new PageInfo<>(mailLogs);
                return ResponseResult.success(count, roleMailLogs.getList());
            }
        } else {
            return ResponseResult.error();
        }


    }

    @Override
    public ResponseResult getMailDetail(long id) {
        //获得邮件日志的详细信息
        MailLog mailLog = mailLogMapper.selectMail(id);
        if (mailLog == null) {
            return ResponseResult.error();
        } else {
            return ResponseResult.success(mailLog);
        }

    }

}
