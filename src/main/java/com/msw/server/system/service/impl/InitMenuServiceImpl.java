package com.msw.server.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msw.server.business.entity.*;
import com.msw.server.business.service.*;
import com.msw.server.common.consts.BusConst;
import com.msw.server.common.enums.StatusEnums;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.common.utils.BookRecordUtils;
import com.msw.server.common.utils.TreeUtil;
import com.msw.server.system.entity.Theme;
import com.msw.server.system.service.IAccessControlService;
import com.msw.server.system.vo.HomeBookVO;
import com.msw.server.system.vo.HomeInfoVO;
import com.msw.server.system.vo.LogoInfoVO;
import com.msw.server.system.entity.Menu;
import com.msw.server.system.mapper.MenuMapper;
import com.msw.server.system.service.InitMenuService;
import com.msw.server.system.vo.MenuVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InitMenuServiceImpl implements InitMenuService {
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private IAccessControlService iAccessControlService;
    @Autowired
    private IManagerBookService iManagerBookService;
    @Autowired
    private IManageRecordService iManageRecordService;
    @Autowired
    private IManageUserConfigService iManageUserConfigService;
    @Autowired
    private IRecommendBooksService iRecommendBooksService;


    @Override
    public MenuVO initMenu(String user_id) {
        MenuVO menuVO = new MenuVO();
        HomeInfoVO homeInfoVO = new HomeInfoVO();
        LogoInfoVO logoInfoVO = new LogoInfoVO();
        List<String> roleIds = (List<String>) iAccessControlService.getRoleIdByUserId(user_id).getData();
        List<Menu> allMenus = menuMapper.selectAllMenu(0);
        homeInfoVO.setTitle("首页").setHref("welcome.html?t=1");
        for (String roleId : roleIds) {
            if (roleId.equals("root")) {
                allMenus = menuMapper.selectAllMenu(1);
                homeInfoVO.setTitle("首页").setHref("console.html?t=1");
                break;
            }
        }
        //调用工具类遍历菜单子父级关系
        List<Menu> menuInfo = TreeUtil.toTree(allMenus, 100);
        logoInfoVO.setTitle("BRS").setImage("images/booklogo.png").setHref("");
        menuVO.setHomeInfo(homeInfoVO).setLogoInfo(logoInfoVO).setMenuInfo(menuInfo);
        iManageRecordService.createScoreCsvFile();
        return menuVO;
    }

    @Override
    public ResponseResult addMenu(Menu menu) {
        int id = menuMapper.selectMaxId();
        menu.setId(id + 1);
        menu.setIcon("fa " + menu.getIcon());
        int i = menuMapper.insertMenu(menu);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }
    }

    @Override
    public ResponseResult queryAllMenu(int page, int limit) {
        int count = menuMapper.selectCountMenu(3);
        if (count == 0) {
            return ResponseResult.error(count, null);
        } else {
            PageHelper.startPage(page, limit);
            List<Menu> menus = menuMapper.selectAllMenu(3);
            PageInfo<Menu> roleMenus = new PageInfo<>(menus);
            return ResponseResult.success(count, roleMenus.getList());
        }
    }

    @Override
    public Theme initTheme(String user_id) {
        Theme theme = menuMapper.selectTheme(user_id);
        return theme;
    }

    @Override
    public ResponseResult makeTheme(Theme theme, String user_id) {
        int i = menuMapper.updateTheme(user_id, theme.getHeaderRightBg(), theme.getLeftMenuBg(), theme.getLeftMenuBgThis(), theme.getLeftMenuChildBg());
        if (i == 1) {
            return ResponseResult.success(StatusEnums.SYS_THEME_MAKE_SUCCESS);
        } else {
            return ResponseResult.error(StatusEnums.SYS_THEME_MAKE_ERROR);
        }

    }

    @Override
    public ResponseResult modifyMenu(Menu menu) {
        int i = menuMapper.updateMenu(menu);
        if (i == 1) {
            return ResponseResult.success();
        } else {
            return ResponseResult.error();
        }
    }

    @Override
    public ResponseResult initHomeInfo(String user_id) throws Exception {
        HomeBookVO homeBookVO = new HomeBookVO();
        //底部推荐区
        List<BookHouse> recommendAreaList = (List<BookHouse>) iRecommendBooksService.useRecommendApi(user_id).getData();
        BookHouse[] recommendArea = new BookHouse[recommendAreaList.size()];
        recommendAreaList.toArray(recommendArea);
        homeBookVO.setRecommendArea(recommendArea);
        //右下侧推荐区
        if (recommendAreaList.size() <= 0 || recommendAreaList == null) {
            homeBookVO.setMaxScore(null);
            homeBookVO.setMaxRecommend(null);
        } else {
            BookRecordUtils.bookSort(recommendAreaList, BusConst.BUS_BOOK_SCORE);
            homeBookVO.setMaxScore(recommendAreaList.get(0));
            BookRecordUtils.bookSort(recommendAreaList, BusConst.BUS_BOOK_RECOMMEND);
            homeBookVO.setMaxRecommend(recommendAreaList.get(0));
        }
        //右上侧浏览记录
        List<BrowseRecord> yesBrowseList = (List<BrowseRecord>) iManageRecordService.getYesBrowseRecords(user_id).getData();
        BrowseRecord[] yesBrowse = new BrowseRecord[yesBrowseList.size()];
        yesBrowseList.toArray(yesBrowse);
        homeBookVO.setYesBrowse(yesBrowse);
        //顶部轮播图
        List<BookHouse> carouselList = (List<BookHouse>) iManagerBookService.getNumSoreBooks(BusConst.BUS_BOOK_PUT_TIME, new BookHouse()).getData();
        BookHouse[] carousel = new BookHouse[carouselList.size()];
        carouselList.toArray(carousel);
        homeBookVO.setCarousel(carousel);
        //返回
        return ResponseResult.success(homeBookVO);
    }


}
