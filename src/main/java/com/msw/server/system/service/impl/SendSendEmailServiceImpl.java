package com.msw.server.system.service.impl;

import com.msw.server.system.service.SendEmailService;
import com.msw.server.business.entity.BookHouse;
import com.msw.server.business.entity.UserInfo;
import com.msw.server.business.service.IManageRecordService;
import com.msw.server.business.service.IManageUserService;
import com.msw.server.business.service.IManagerBookService;
import com.msw.server.common.consts.SysConst;
import com.msw.server.common.enums.StatusEnums;
import com.msw.server.common.param.*;
import com.msw.server.common.utils.EmailUtils;
import com.msw.server.system.entity.MailLog;
import com.msw.server.system.service.ILogGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SendSendEmailServiceImpl implements SendEmailService {
//    @Autowired
//    private EmailUtils emailUtils;
    @Autowired
    private ILogGenerateService iLogGenerateService;
    @Autowired
    private IManageUserService iManageUserService;
    @Autowired
    private IManageRecordService iManageRecordService;
    @Autowired
    private IManagerBookService iManagerBookService;

    @Override
    public ResponseResult sendTextMail(EmailParam emailParam, String user_id) {
        //创建推荐记录
        String books = emailParam.getBooks();
        String[] books_id = books.split(",");
        List<BookHouse> bookList = new ArrayList<>();
        for (String book_id : books_id) {
            BookHouse book = (BookHouse) iManagerBookService.getBooksByBookId(book_id).getData();
            bookList.add(book);
            iManageRecordService.createRecommendRecord(book_id, user_id);
        }
        //发送邮件
        //emailUtils.sendHtmlMail(emailParam.getAddress(), emailParam.getSubject(), EmailUtils.getEmailContent(bookList));
        UserInfo sender = (UserInfo) iManageUserService.getUserInfo(user_id).getData();
        UserInfo receiver = (UserInfo) iManageUserService.getUserInfoByMail(emailParam.getAddress()).getData();
        //创建邮件日志
        MailLog mailLog = new MailLog(0, user_id, sender.getName(), sender.getE_mail(), receiver.getUser_id()
                , receiver.getName(), receiver.getE_mail(), emailParam.getSubject(), emailParam.getContent(), emailParam.getFilepath(), SysConst.SYS_SUCCESS_STATUS, new Date());
        ResponseResult result = iLogGenerateService.createMailLog(mailLog);
        if (result.getCode() == 200) {
            return ResponseResult.success(StatusEnums.SYS_SEND_MAIL_SUCCESS);
        } else {
            return ResponseResult.error(StatusEnums.SYS_SEND_MAIL_ERROR);
        }

    }

//    @Override
//    public ResponseResult sendFileMail(EmailParam emailParam) {
//        emailUtils.sendAttachmentMail(emailParam.getAddress(), emailParam.getSubject(), emailParam.getContent(), emailParam.getFilepath());
//        return ResponseResult.success();
//    }
}
