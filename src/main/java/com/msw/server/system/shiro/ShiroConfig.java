package com.msw.server.system.shiro;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@ClassAnnotation("权限配置类")
@Configuration
public class ShiroConfig {

    @MethodAnnotation("地址过滤器")
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager defaultWebSecurityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(defaultWebSecurityManager);
        // 设置登录url
        shiroFilterFactoryBean.setLoginUrl("/sys/brs-login");
        // 设置未授权的url
        shiroFilterFactoryBean.setUnauthorizedUrl("/403");
        Map<String, String> filterMap = new HashMap<>();
        // 注销登录
        filterMap.put("/sys/exitLogin", "anon");
        // 开放登录接口
        filterMap.put("/sys/login", "anon");
        // 开放注册接口
        filterMap.put("/sys/register", "anon");
        // 开放注册页面
        filterMap.put("/system/register.html", "anon");
        // 开放获取验证码接口
        filterMap.put("/kaptcha-image", "anon");
        // 开放静态页面
        filterMap.put("/css/**", "anon");
        filterMap.put("/images/**", "anon");
        filterMap.put("/js/**", "anon");
        filterMap.put("/lib/**", "anon");
        // 其余url全部拦截，必须放在最后
        filterMap.put("/**", "authc");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);
        return shiroFilterFactoryBean;
    }

    @MethodAnnotation("安全管理器")
    @Bean
    public DefaultWebSecurityManager defaultWebSecurityManager(UserRealm userRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(userRealm);
        return securityManager;
    }

    @MethodAnnotation("创建Realm对象")
    @Bean
    public UserRealm userRealm() {
        return new UserRealm();
    }
}
