package com.msw.server.system.shiro;


import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.MethodAnnotation;
import com.msw.server.common.param.ResponseResult;
import com.msw.server.system.entity.User;
import com.msw.server.system.service.IAccessControlService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@ClassAnnotation("授权与认证")
public class UserRealm extends AuthorizingRealm {
    @Autowired
    private IAccessControlService IAccessControlService;

    @MethodAnnotation("授权")
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        User user = (User) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        if (user != null && user.getUser_id().equals("root")) {
            simpleAuthorizationInfo.addRole("administrator");
            simpleAuthorizationInfo.addStringPermission("root:root");
        } else {
            //查询用户的角色列表
            List<String> roleIdList = (List<String>) IAccessControlService.getRoleIdByUserId(user.getUser_id()).getData();
            if (roleIdList != null) {
                //添加角色
                simpleAuthorizationInfo.addRoles(roleIdList);
                List<String> signList = new ArrayList<>();
                for (String roleId : roleIdList) {
                    //查询权限id
                    List<String> permissionIdList = (List<String>) IAccessControlService.getPIdByRoleId(roleId).getData();
                    if (permissionIdList != null) {
                        for (String pid : permissionIdList) {//查询权限标识
                            String sign = (String) IAccessControlService.getSignByPId(pid).getData();
                            signList.add(sign);
                        }

                    }
                }
                //添加权限
                simpleAuthorizationInfo.addStringPermissions(signList);
            }

        }
        return simpleAuthorizationInfo;
    }

    @MethodAnnotation("认证")
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken userToken = (UsernamePasswordToken) authenticationToken;
        //得到用户信息
        User user = (User) IAccessControlService.getUserByUsername(userToken.getUsername()).getData();
        if (user == null) {
            return null;
        }
        return new SimpleAuthenticationInfo(user, user.getPassword(), "");
    }
}
