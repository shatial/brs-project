package com.msw.server.system.vo;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
@ApiModel("缓存VO")
@Data
@Accessors(chain = true)
public class ClearCacheVO {
    @ApiModelProperty("code")
    private int code;

    @ApiModelProperty("信息")
    private String msg;
}
