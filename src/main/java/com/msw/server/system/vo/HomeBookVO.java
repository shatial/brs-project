package com.msw.server.system.vo;

import com.msw.server.business.entity.BookHouse;
import com.msw.server.business.entity.BrowseRecord;
import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.common.annotation.PropertyAnnotation;
import lombok.Data;
import lombok.ToString;


@ClassAnnotation("首页VO")
@Data
@ToString
public class HomeBookVO {
    @PropertyAnnotation("顶部轮播图")
    private BookHouse[] carousel;

    @PropertyAnnotation("底部图书推荐")
    private BookHouse[] recommendArea;

    @PropertyAnnotation("右侧昨日浏览记录")
    private BrowseRecord[] yesBrowse;

    @PropertyAnnotation("右下评分最高")
    private BookHouse maxScore;

    @PropertyAnnotation("右下推荐数最多")
    private BookHouse maxRecommend;
}
