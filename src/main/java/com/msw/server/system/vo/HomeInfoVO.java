package com.msw.server.system.vo;

import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel("主页VO")
@Data
@Accessors(chain = true)
public class HomeInfoVO {
    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("路径")
    private String href;
}
