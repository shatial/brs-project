package com.msw.server.system.vo;

import com.msw.server.common.annotation.ClassAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel("logoVO")
@Data
@Accessors(chain = true)
public class LogoInfoVO {
    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("图片")
    private String image;

    @ApiModelProperty("路径")
    private String href;
}
