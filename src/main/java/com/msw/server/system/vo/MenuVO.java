package com.msw.server.system.vo;

import com.msw.server.common.annotation.ClassAnnotation;
import com.msw.server.system.entity.Menu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@ApiModel("菜单VO")
@Data
@Accessors(chain = true)
public class MenuVO {
    @ApiModelProperty("主页")
    private HomeInfoVO homeInfo;

    @ApiModelProperty("logo")
    private LogoInfoVO logoInfo;

    @ApiModelProperty("菜单")
    private List<Menu> menuInfo;

}
