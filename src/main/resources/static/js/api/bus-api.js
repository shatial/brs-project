const busURL = 'http://localhost:8080/bus';

const perfectUserInfoURL = busURL + '/perfectUserInfo';

const getUserInfoURL = busURL + '/getUserInfo';

const createBrowseRecordURL = busURL + '/createBrowseRecord';

const getBrowseRecordsURL = busURL + '/getBrowseRecords';

const deleteBrowseRecordsURL = busURL + '/deleteBrowseRecords';

const getCollectRecordsURL = busURL + '/queryCollectRecords';

const createCollectRecordURL = busURL + '/createCollectRecord';

const deleteCollectRecordsURL = busURL + '/deleteCollectRecords';

const getRecycleBinsURL = busURL + '/queryRecycleBins';

const deleteRecycleBinsURL = busURL + '/deleteRecycleBins';

const getCommentRecordsURL = busURL + '/queryCommentRecords';

const deleteCommentRecordsURL = busURL + '/deleteCommentRecords';

const updateContentURL = busURL + '/updateContent';

const getScoreRecordsURL = busURL + '/queryScoreRecords';

const createScoreRecordURL = busURL + '/createScoreRecord';

const deleteScoreRecordsURL = busURL + '/deleteScoreRecords';

const updateScoreURL = busURL + '/updateScore';

const createUploadRecordsURL = busURL + '/createUploadRecords';

const checkUploadRecordURL = busURL + '/checkUploadRecord';

const getUploadRecordsURL = busURL + '/queryUploadRecords';

const deleteUploadRecordsURL = busURL + '/deleteUploadRecords';

const createCommentRecordURL = busURL + '/createCommentRecord';

const getRecommendRecordsURL = busURL + '/queryRecommendRecords';

const deleteRecommendRecordsURL = busURL + '/deleteRecommendRecords';

const revokeRecommendURL = busURL + "/revokeRecommend";

const getWeekGraphLineURL = busURL + '/getWeekGraphLine';

const getCollectRoseGraphURL = busURL + '/getCollectRoseGraph';

const getUserConfigURL = busURL + '/getUserConfig';

const setUserConfigURL = busURL + '/setUserConfig';

const sortCollectTypeURL = busURL + '/sortCollectType';

const getTypeBookRecommendURL = busURL + '/getTypeBookRecommend';

const queryParentBookTypeURL = busURL + '/queryParentBookType';

const queryChildBookTypeURL = busURL + '/queryChildBookType';

const getParentChildListURL = busURL + '/getParentChildList';

const queryParentWorkTypeURL = busURL + '/queryParentWorkType';

const queryBooksURL = busURL + '/queryBooks';

const toBookDetailsURL = busURL + '/toBookDetails';

const getBookDetailsURL = busURL + '/getBookDetails';

