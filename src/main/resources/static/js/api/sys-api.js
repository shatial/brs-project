const sysURL = 'http://localhost:8080/sys';

const loginURL = sysURL + '/login';

const exitURL = sysURL + '/exitLogin';

const registerURL = sysURL + '/register';

const modifyPasswordURL = sysURL + '/modifyPassword';

const initMenuUrl = sysURL + '/initMenu';   // 初始化接口

const addMenuURL = sysURL + '/addMenu';

const getMenusURL = sysURL + '/getMenus';

const initThemeURL = sysURL + '/initTheme';

const makeThemeURL = sysURL + '/makeTheme';

const initHomeInfoURL = sysURL + '/initHomeInfo';

const clearCacheUrl = sysURL + '/clear';

const getMailURL = sysURL + '/getMails';

const delMailsURL = sysURL + '/delMails';

const getMailDetailURL = sysURL + '/getMailDetail';

const queryMonitorLogURL = sysURL + '/queryMonitorLog';

const clearMonitorLogURL = sysURL + '/clearMonitorLog';

const queryAllUserURL = sysURL + '/queryAllUser';

const queryAllRolesURL = sysURL + '/queryAllRoles';

const queryAllPermissionsURL = sysURL + '/queryAllPermissions';

const cancelOrRestoreUserURL = sysURL + '/cancelOrRestoreUser';

const editRoleURL = sysURL + '/editRole';

const editPermissionURL = sysURL + '/editPermission';

const editMenuURL = sysURL + '/editMenu';

const queryUserAssignableRoleURL = sysURL + '/queryUserAssignableRole';

const addUserRoleURL = sysURL + '/addUserRole';

const queryRoleAssignablePermissionURL = sysURL + '/queryRoleAssignablePermission';

const addRolePermissionURL = sysURL + '/addRolePermission';


