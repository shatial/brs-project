
(function (win) {
  'use strict'
  if (!Raphael) {
    throw new Error('需要Raphael.js')
  }
  var clone = function (obj) {
    var newObject = {};
    for (var key in obj) {
      if (typeof obj[key] === 'number' || typeof obj[key] === 'boolean' || typeof obj[key] === 'string' || obj[key] === undefined || obj[key] === null) {
        newObject[key] = obj[key];
      } else {
        newObject[key] = clone(obj[key])
      }
    }
    return newObject;
  }

  if (typeof Object.assign !== 'function') {
    Object.assign = function (target) {
      if (target === null) {
        throw new Error('Cannot convert undefined or null to object')
      }
      target = Object(target)
      for (var index = 1; index < arguments.length; index++) {
        var source = arguments[index]
        if (source !== null) {
          for (var key in source) {
            if (Object.prototype.hasOwnProperty.call(source, key)) {
              target[key] = source[key]
            }
          }
        }
      }
      return target
    }
  }
  var defaults = {
    type: 'loop',
    value: 96.6,
    background: '#CCCCCC',
    color: '#00A0E9',
    isText: true,
    textColor: '#00A0E9',
    textPosition: {
      x: null,
      y: null
    },
    fontSize: 20,
    fontWeight: 700,
    width: 20,
    radius: 60,
    deg: 0,
    time: 1000,
    easing: 'linear'
  }
  var Progress = function (el, option) {
    this.el = el
    this.pr = null
    this.option = Object.assign({}, clone(defaults), option)
    this.create()
  }
  Progress.prototype.create = function () {
    this.option.value = Math.floor(this.option.value * 100) / 100
    if (!typeof this.el === 'object') {
      throw new Error('请传入DOM对象')
    }
    if (this.option.value < 0 || this.option.value > 100) {
      throw new Error('请输入0~100之间的数')
    }
    if (this.option.type === 'loop') {
      this.option.textPosition.x = this.option.textPosition.x ? this.option.textPosition.x : this.option.width + this.option.radius
      this.option.textPosition.y = this.option.textPosition.y ? this.option.textPosition.y : this.option.width + this.option.radius
      var r = this.option.radius
      var time = this.option.time
      var val = this.option.value
      var xy = r + this.option.width
      this.pr = Raphael(this.el, (r + this.option.width) * 2, (r + this.option.width) * 2)
      this.pr.circle(xy, xy, r).attr({
        'stroke-width': this.option.width,
        stroke: this.option.background
      })
      this.pr.customAttributes.arc = function (value) {
        var v = 360 * value / 100,
          s = -180,
          e = s + v,
          x = xy,
          y = xy,
          rad = Math.PI / 180,
          x1 = x + r * Math.sin(-s * rad),
          y1 = y + r * Math.cos(-s * rad),
          x2 = x + r * Math.sin(-e * rad),
          y2 = y + r * Math.cos(-e * rad),
          path = [
            ['M', x1, y1],
            ['A', r, r, 0, +(e - s > 180), 1, x2, y2]
          ]
        return {
          path: path
        }
      }
      var an = this.pr.path().attr({
        'stroke-width': this.option.width,
        stroke: this.option.color,
        arc: 0
      })
      if (this.option.deg) {
        an.rotate(this.option.deg, xy, xy)
      }
      an.animate({
        stroke: this.option.color,
        arc: val
      }, time, this.option.easing)
      var that = this
      var timer = setTimeout(function () {
        if (val == 100) {
          that.pr.circle(xy, xy, r).attr({
            'stroke-width': that.option.width,
            stroke: that.option.color
          })
        }
        clearTimeout(timer)
      }, time)
      if (this.option.isText) {
        this.pr.customAttributes.txt = function (val) {
          return {
            text: Math.floor(val * 100) / 100 + '%'
          }
        }
        var l = this.pr.text(this.option.textPosition.x, this.option.textPosition.y).attr({
          txt: 0,
          'fill': this.option.textColor,
          'font-size': this.option.fontSize + 'px',
          'font-weight': this.option.fontWeight
        })
        l.animate({
          txt: val
        }, time)
      }
    } else if (this.option.type === 'bar') {
      this.option.textColor = '#FFFFFF'
      this.option.radius = 300
      this.option.textPosition.x = this.option.textPosition.x ? this.option.textPosition.x : this.option.radius / 2
      this.option.textPosition.y = this.option.textPosition.y ? this.option.textPosition.y : this.option.width / 2
      var size = this.option.width
      var s = size / 2
      var l = this.option.radius
      var val = this.option.value
      this.pr = Raphael(this.el, l, size)
      this.pr.path('M 0,' + s + ' L' + l + ',' + s).attr({
        'stroke-width': size,
        stroke: this.option.background
      })
      var strip = this.pr.path('M 0,' + s + ' L0,' + s).attr({
        'stroke-width': size,
        stroke: this.option.color
      })
      strip.animate({
        path: 'M 0,' + s + ' L' + l / 100 * val + ',' + s
      }, this.option.time, this.option.easing)
      if (this.option.isText) {
        this.pr.customAttributes.arc = function (val) {
          return {
            text: Math.floor(val * 100) / 100 + '%'
          }
        }
        var l = this.pr.text(this.option.textPosition.x, this.option.textPosition.y).attr({
          arc: 0,
          'fill': this.option.textColor,
          'font-size': this.option.fontSize
        })
        l.animate({
          arc: val
        }, this.option.time)
      }
    } else {
      throw new Error('请输入正确的类型"loop"或者"bar"')
    }
  }
  Progress.prototype.update = function (val) {
    var val = Math.floor(val * 100) / 100
    if (val < 0 || val > 100) {
      throw new Error('请输入0~100之间的数')
    }
    if (this.option.type === 'loop') {
      var r = this.option.radius
      var xy = r + this.option.width
      this.pr.clear()
      this.pr.circle(xy, xy, r).attr({
        'stroke-width': this.option.width,
        stroke: this.option.background
      })
      this.pr.customAttributes.txt = function (val) {
        return {
          text: Math.floor(val * 100) / 100 + '%'
        }
      }
      var an = this.pr.path().attr({
        txt: this.option.value,
        'font-size': this.option.fontSize + 'px',
        'font-weight': this.option.fontWeight,
        'stroke-width': this.option.width,
        stroke: this.option.color,
        arc: this.option.value
      })
      if (this.option.deg) {
        an.rotate(this.option.deg, xy, xy)
      }
      an.animate({
        txt: val,
        stroke: this.option.color,
        arc: val
      }, this.option.time, this.option.easing)
      var that = this
      var timer = setTimeout(function () {
        if (val == 100) {
          that.pr.circle(xy, xy, r).attr({
            'stroke-width': that.option.width,
            stroke: that.option.color
          })
        }
        clearTimeout(timer)
      }, this.option.time)
      if (this.option.isText) {
        this.pr.customAttributes.txt = function (val) {
          return {
            text: Math.floor(val * 100) / 100 + '%'
          }
        }
        var l = this.pr.text(this.option.textPosition.x, this.option.textPosition.y).attr({
          txt: this.option.value,
          'fill': this.option.textColor,
          'font-size': this.option.fontSize + 'px',
          'font-weight': this.option.fontWeight
        })
        l.animate({
          txt: val
        }, this.option.time)
        this.option.value = val
      }
    } else if (this.option.type === 'bar') {
      var size = this.option.width
      var s = size / 2
      var l = this.option.radius
      this.pr.clear()
      this.pr.path('M 0,' + s + ' L' + l + ',' + s).attr({
        'stroke-width': size,
        stroke: this.option.background
      })
      var strip = this.pr.path('M 0,' + s + ' L' + l / 100 * this.option.value + ',' + s).attr({
        'stroke-width': size,
        stroke: this.option.color
      })
      strip.animate({
        path: 'M 0,' + s + ' L' + l / 100 * val + ',' + s
      }, this.option.time, this.option.easing)
      if (this.option.isText) {
        this.pr.customAttributes.arc = function (val) {
          return {
            text: Math.floor(val * 100) / 100 + '%'
          }
        }
        var l = this.pr.text(this.option.textPosition.x, this.option.textPosition.y).attr({
          arc: this.option.value,
          'fill': this.option.textColor,
          'font-size': this.option.fontSize
        })
        l.animate({
          arc: val
        }, this.option.time)
      }
      this.option.value = val
    }
  }
  win.Progress = Progress
})(window)